/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Daniel
 */
public class Lector {
    private int id_lect;
    private String ubic_lect;

    public int getId_lect() {
        return id_lect;
    }

    public void setId_lect(int id_lect) {
        this.id_lect = id_lect;
    }

    public String getUbic_lect() {
        return ubic_lect;
    }

    public void setUbic_lect(String ubic_lect) {
        this.ubic_lect = ubic_lect;
    }
    
    
}
