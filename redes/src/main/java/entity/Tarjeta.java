/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Daniel
 */
public class Tarjeta {

    private int id_tarj;
    private String codTarjeta;
    private String esta_tarj;
    private String tipo_tarj;

    public int getId_tarj() {
        return id_tarj;
    }

    public void setId_tarj(int id_tarj) {
        this.id_tarj = id_tarj;
    }

    public String getCodTarjeta() {
        return codTarjeta;
    }

    public void setCodTarjeta(String codTarjeta) {
        this.codTarjeta = codTarjeta;
    }

    public String getEsta_tarj() {
        return esta_tarj;
    }

    public void setEsta_tarj(String esta_tarj) {
        this.esta_tarj = esta_tarj;
    }

    public String getTipo_tarj() {
        return tipo_tarj;
    }

    public void setTipo_tarj(String tipo_tarj) {
        this.tipo_tarj = tipo_tarj;
    }

    
}
