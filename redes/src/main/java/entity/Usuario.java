/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Daniel
 */
public class Usuario {

    private String nomb_user;
    private String pass_user;
    private Persona persona;

    public String getNomb_user() {
        return nomb_user;
    }

    public void setNomb_user(String nomb_user) {
        this.nomb_user = nomb_user;
    }

    public String getPass_user() {
        return pass_user;
    }

    public void setPass_user(String pass_user) {
        this.pass_user = pass_user;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    

}
