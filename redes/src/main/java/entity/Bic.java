/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Daniel
 */
public class Bic {

    private int codigo;
    private String fecha;
    private String usuario;
    private String nomb_table;
    private String oper;
    private String valor_v;
    private String valor_n;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNomb_table() {
        return nomb_table;
    }

    public void setNomb_table(String nomb_table) {
        this.nomb_table = nomb_table;
    }

    public String getOper() {
        return oper;
    }

    public void setOper(String oper) {
        this.oper = oper;
    }

    public String getValor_v() {
        return valor_v;
    }

    public void setValor_v(String valor_v) {
        this.valor_v = valor_v;
    }

    public String getValor_n() {
        return valor_n;
    }

    public void setValor_n(String valor_n) {
        this.valor_n = valor_n;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

}
