/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Javox
 */
public class Persona_tarjeta {
    private String fech_asig;
    private String fech_devo;
    private Persona pers;
    private Tarjeta tarj;

    public String getFech_asig() {
        return fech_asig;
    }

    public void setFech_asig(String fech_asig) {
        this.fech_asig = fech_asig;
    }

    public String getFech_devo() {
        return fech_devo;
    }

    public void setFech_devo(String fech_devo) {
        this.fech_devo = fech_devo;
    }

    public Persona getPers() {
        return pers;
    }

    public void setPers(Persona pers) {
        this.pers = pers;
    }

    public Tarjeta getTarj() {
        return tarj;
    }

    public void setTarj(Tarjeta tarj) {
        this.tarj = tarj;
    }

}
