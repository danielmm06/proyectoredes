/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Daniel
 */
public class Rol {
    private int id_rol;
    private String nomb_rol;

    public int getId_rol() {
        return id_rol;
    }

    public void setId_rol(int id_rol) {
        this.id_rol = id_rol;
    }

    public String getNomb_rol() {
        return nomb_rol;
    }

    public void setNomb_rol(String nomb_rol) {
        this.nomb_rol = nomb_rol;
    }
    
}
