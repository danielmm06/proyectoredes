/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author Daniel
 */
public class Registro {
    
    private int id_regi;
    private String fech_regi;
    private String hora_regi;
    private Lector lector;
    private Tarjeta tarjeta;
    private Persona persona;

    public int getId_regi() {
        return id_regi;
    }

    public void setId_regi(int id_regi) {
        this.id_regi = id_regi;
    }

    public String getFech_regi() {
        return fech_regi;
    }

    public void setFech_regi(String fech_regi) {
        this.fech_regi = fech_regi;
    }

    public String getHora_regi() {
        return hora_regi;
    }

    public void setHora_regi(String hora_regi) {
        this.hora_regi = hora_regi;
    }

    public Lector getLector() {
        return lector;
    }

    public void setLector(Lector lector) {
        this.lector = lector;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    
}
