﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import conexion.App;
import conexion.DataBase;
import entity.Persona;
import entity.Rol;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class PersonaDAO {
private DataBase db;

    public PersonaDAO() {
        if (App.DB != null) {
            this.db = App.DB;
        } else {
            throw new RuntimeException("Error: No se ha inicializado la conexión.");
        }
    }

   public ArrayList<Persona> getAll() {
        ArrayList<Persona> listaPersona = new ArrayList<Persona>();
        ResultSet result = null;
        PreparedStatement psSelectAll = null;
        try {
            if (psSelectAll == null) {
                psSelectAll = db.PreparedQuery("select id_pers, nomb_pers, apell_pers, num_documento, id_rol from persona");
            }
            result = db.ExecuteQuery(psSelectAll);
            while (result.next()) {
                Persona persona = new Persona();
                persona.setId_pers(result.getInt("id_pers"));
                persona.setNomb_pers(result.getString("nomb_pers"));
                persona.setApell_pers(result.getString("apell_pers"));
                persona.setNum_documento(result.getInt("num_documento"));
                

                Rol rol = new Rol();
                rol.setId_rol(result.getInt("id_rol"));
                persona.setRol(rol);
                
                listaPersona.add(persona);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectAll != null) {
                try {
                    psSelectAll.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaPersona;
    }

    public Persona get(int id_pers) {
        Persona persona = new Persona();
        ResultSet result = null;
        PreparedStatement psSelect = null;
        try {
            if (psSelect == null) {
                psSelect = db.PreparedQuery(
                        "select id_pers, nomb_pers, apell_pers, num_documento, id_rol from persona where id_pers = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(id_pers);
            result = db.ExecuteQuery(psSelect, inputs);
            while (result.next()) {
                persona.setId_pers(result.getInt("id_pers"));
                persona.setNomb_pers(result.getString("nomb_pers"));
                persona.setApell_pers(result.getString("apell_pers"));
                persona.setNum_documento(result.getInt("num_documento"));

                Rol rol = new Rol();
                rol.setId_rol(result.getInt("id_rol"));
                persona.setRol(rol);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelect != null) {
                try {
                    psSelect.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return persona;
    }

    public long insert(Persona persona) {
        PreparedStatement psInsert = null;
        long result;
        try {

            String columns = "id_pers, nomb_pers, apell_pers, num_documento, id_rol";
            String values = "?,?,?,?";
            if (psInsert == null) {
                psInsert = db.PreparedUpdate(
                        "INSERT INTO persona(" + columns + ") VALUES(" + values + ")",
                        "id_pers"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();

            if (persona.getNomb_pers()!= null) {
                inputs.add(persona.getNomb_pers());
            }
            if (persona.getApell_pers() != null) {
                inputs.add(persona.getApell_pers());
            }
            if (persona.getNum_documento() != 0) {
                inputs.add(persona.getNum_documento());
            }
            if(persona.getRol() != null){
                inputs.add(persona.getRol().getId_rol());
            }
            
            result = db.ExecuteUpdate(psInsert, inputs);

        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar inserción.", e);
        } finally {
            if (psInsert != null) {
                try {
                    psInsert.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long update(Persona persona) {
        PreparedStatement psUpdate = null;
        long result;
        try {
            String columns = "";
            ArrayList<Object> inputs = new ArrayList<Object>();
            
            if (persona.getNomb_pers() != null) {
                columns += ",nomb_pers=?";
                inputs.add(persona.getNomb_pers());
            }
            if (persona.getApell_pers() != null) {
                columns += ",apell_pers=?";
                inputs.add(persona.getApell_pers());
            }
            if (persona.getNum_documento()!= 0) {
                columns += ",num_documento=?";
                inputs.add(persona.getNum_documento());
            }
            if (persona.getRol() != null) {
                columns += ",id_tarj=?";
                inputs.add(persona.getRol().getId_rol());
            }

            inputs.add(persona.getId_pers());
            columns = columns.substring(1);

            psUpdate = db.PreparedUpdate(
                    "UPDATE persona SET " + columns + " WHERE id_pers=?"
            );
            result = db.ExecuteUpdate(psUpdate, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar actualización.", e);
        } finally {
            if (psUpdate != null) {
                try {
                    psUpdate.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long delete(int id_pers) {
        PreparedStatement psDelete = null;
        long result;
        try {
            if (psDelete == null) {
                psDelete = db.PreparedUpdate(
                        "DELETE FROM persona "
                        + "WHERE id_pers=?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(id_pers);
            result = db.ExecuteUpdate(psDelete, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar borrado.", e);
        } finally {
            if (psDelete != null) {
                try {
                    psDelete.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }
    
    public Persona getDoc(String doc) {
        Persona persona = new Persona();
        ResultSet result = null;
        PreparedStatement psSelectDoc = null;
        try {
            if (psSelectDoc == null) {
                psSelectDoc = db.PreparedQuery(
                        "select id_pers, nomb_pers, apell_pers, num_documento, id_rol from persona where num_documento = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(doc);
            result = db.ExecuteQuery(psSelectDoc, inputs);
            while (result.next()) {
                persona.setId_pers(result.getInt("id_pers"));
                persona.setNomb_pers(result.getString("nomb_pers"));
                persona.setApell_pers(result.getString("apell_pers"));
                persona.setNum_documento(result.getInt("num_documento"));

                Rol rol = new Rol();
                rol.setId_rol(result.getInt("id_rol"));
                persona.setRol(rol);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectDoc != null) {
                try {
                    psSelectDoc.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return persona;
    }

    public static void main(String[] args) throws IOException {
        App App = new App();
        try {
            App.OpenConnection();
            // Se prueban los metodos del CRUD
        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            App.CloseConnection();
        }
    }
}
