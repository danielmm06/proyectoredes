﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import conexion.App;
import conexion.DataBase;
import entity.Persona;
import entity.Usuario;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class UsuarioDAO {
    private DataBase db;

    public UsuarioDAO() {
        if (App.DB != null) {
            this.db = App.DB;
        } else {
            throw new RuntimeException("Error: No se ha inicializado la conexión.");
        }
    }
    
    public ArrayList<Usuario> getAll() throws SQLException {
        ArrayList<Usuario> listaUsuario = new ArrayList<Usuario>();
        PreparedStatement psSelectAll = null;
        ResultSet result = null;
        try {
            if (psSelectAll == null) {
                psSelectAll = db.PreparedQuery(
                        "SELECT nomb_user, pass_user, id_pers "
                        + "FROM usuario"
                );
            }
            result = db.ExecuteQuery(psSelectAll);
            while (result.next()) {
                Usuario usuarios = new Usuario();
                usuarios.setNomb_user(result.getString("nomb_user"));
                usuarios.setPass_user(result.getString("pass_user"));
                Persona persona = new Persona();
                persona.setId_pers(result.getInt("id_pers"));
                usuarios.setPersona(persona);

                listaUsuario.add(usuarios);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectAll != null) {
                try {
                    psSelectAll.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaUsuario;
    }

    public Usuario get(String nomb_user) {
        Usuario usuarios = new Usuario();
        PreparedStatement psSelect = null;
        ResultSet result = null;
        try {
            if (psSelect == null) {
                psSelect = db.PreparedQuery(
                        "SELECT nomb_user, pass_user, id_pers FROM usuario WHERE nomb_user=?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(nomb_user);
            result = db.ExecuteQuery(psSelect, inputs);
            while (result.next()) {
                usuarios.setNomb_user(result.getString("nomb_user"));
                usuarios.setPass_user(result.getString("pass_user"));
                Persona persona = new Persona();
                persona.setId_pers(result.getInt("id_pers"));
                usuarios.setPersona(persona);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelect != null) {
                try {
                    psSelect.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return usuarios;
    }
    

    public long insert(Usuario usuarios) {
        PreparedStatement psInsert = null;
        long result;
        try {
            String columns = "nomb_user, pass_user, id_pers";
            String values = "?,?,?";
            if (psInsert == null) {
                psInsert = db.PreparedUpdate(
                        "INSERT INTO usuario(" + columns + ") VALUES(" + values + ")", "nomb_user"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            if (usuarios.getNomb_user() != null) {
                inputs.add(usuarios.getNomb_user());
            }
            if (usuarios.getPass_user() != null) {
                inputs.add(usuarios.getPass_user()); 
            } else {
                inputs.add(null);
            }
            if (usuarios.getPersona() != null) {
                inputs.add(usuarios.getPersona().getId_pers());
            }

            result = db.ExecuteUpdate(psInsert, inputs);

        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar inserción.", e);
        } finally {
            if (psInsert != null) {
                try {
                    psInsert.close();
                    psInsert = null;
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long update(Usuario usuarios) {
        PreparedStatement psUpdate = null;
        long result;
        try {
            String columns = "";
            ArrayList<Object> inputs = new ArrayList<Object>();
            
            if (usuarios.getPass_user() != null) {
                columns += ",pass_user=?";
                inputs.add(usuarios.getPass_user()); 
//                inputs.add(DigestUtils.md5Hex(usuarios.getContrasena()));
            }
            if (usuarios.getPersona() != null) {
                columns += ",id_pers=?";
                inputs.add(usuarios.getPersona().getId_pers());
            }

            inputs.add(usuarios.getNomb_user());
            columns = columns.substring(1);

            if (psUpdate == null) {
                psUpdate = db.PreparedUpdate(
                        "UPDATE usuario SET " + columns + " WHERE nomb_user=?"
                );
            }
            result = db.ExecuteUpdate(psUpdate, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar actualización.", e);
        } finally {
            if (psUpdate != null) {
                try {
                    psUpdate.close();
                    psUpdate = null;
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long delete(String nomb_user) {
        PreparedStatement psDelete = null;
        long result;
        try {
            if (psDelete == null) {
                psDelete = db.PreparedUpdate(
                        "DELETE FROM usuario "
                        + "WHERE nomb_user=?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(nomb_user);
            result = db.ExecuteUpdate(psDelete, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar borrado.", e);
        } finally {
            if (psDelete != null) {
                try {
                    psDelete.close();
                    psDelete = null;
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }
   

    public static void main(String[] args) throws IOException {
        App App = new App();
        try {
            App.OpenConnection();
            // Se prueban los metodos del CRUD
            
//          System.out.println("GET ALL");
//            ArrayList<Usuarios> listaUsuarios = App.UsuariosDAO.getAll();
//            for (Usuarios listaUsuario : listaUsuarios) {
//                System.out.println(listaUsuario.getIdUsuario() + " " + listaUsuario.getContrasena()+" "+listaUsuario.getIdRol().getIdTUsuario());
//            }
//			System.out.println("GET ONE");
//			int idUsuario = 1120654554;			
//			Usuarios usuarios = App.UsuariosDAO.get(idUsuario);
//			if(usuarios.getIdUsuario() != 0) {						
//				System.out.println(usuarios.getIdUsuario()+" "+usuarios.getContrasena()+" "+usuarios.getIdRol().getIdTUsuario());
//			}


//                        System.out.println("GET ONE");
//			String idUsuario = "1121";
////                        String contrasena = "daniel";
//			Usuarios usuarios = App.UsuariosDAO.getnombre(idUsuario);
//			if(usuarios.getIdUsuario() != 0) {						
//                            System.out.println(usuarios.getIdUsuario()+" "+usuarios.getContrasena());
//			}
//                        System.out.println("INSERT");				
//                        Usuario usuarios = new Usuario();
//                        usuarios.setNomb_user("daniel");
//                        usuarios.setPass_user("daniel123");
//                        Persona persona = new PersonaDAO().get(1);
//                        System.out.println(persona.getNomb_pers());
//                        usuarios.setPersona(persona);
//                        App.UsuarioDAO.insert(usuarios);	
//                    
//                    System.out.println("UPDATE");				
//                    Usuarios usuarios = new Usuarios();
//                    usuarios.setIdUsuario(112166);
//                    usuarios.setContrasena("daniel3");
//                    Rol rol = new RolDAO().get(1);
//                    usuarios.setIdRol(rol);
//                    App.UsuariosDAO.update(usuarios);
//                    System.out.println(App.UsuariosDAO.update(usuarios));
//                      System.out.println("DELETE");
//                      App.UsuariosDAO.delete(1120654554);
//                      System.out.println(App.RolDAO.delete(1120654554));  
          
        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            App.CloseConnection();
        }
    }
}
