/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import conexion.App;
import conexion.DataBase;
import entity.Tarjeta;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class TarjetaDAO {

    private DataBase db;

    public TarjetaDAO() {
        if (App.DB != null) {
            this.db = App.DB;
        } else {
            throw new RuntimeException("Error: No se ha inicializado la conexión.");
        }
    }

    public Tarjeta getIdTarjeta(String codTarjeta) {
        Tarjeta tarjeta = new Tarjeta();
        ResultSet result = null;
        PreparedStatement psSelect = null;
        try {
            if (psSelect == null) {
                psSelect = db.PreparedQuery(
                        "select id_tarj, esta_tarj, tipo_tarj, cod_tarjeta from tarjeta where cod_tarjeta = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(codTarjeta);
            result = db.ExecuteQuery(psSelect, inputs);
            while (result.next()) {
                tarjeta.setId_tarj(result.getInt("id_tarj"));
                tarjeta.setCodTarjeta(result.getString("cod_tarjeta"));               
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelect != null) {
                try {
                    psSelect.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return tarjeta;
    }
    
    public ArrayList<Tarjeta> getAll(){
        ArrayList<Tarjeta> listaTarjeta = new ArrayList<Tarjeta>();
        PreparedStatement psSelectAll = null;
        ResultSet result = null;
        try {
            if (psSelectAll == null) {
                psSelectAll = db.PreparedQuery(
                        "SELECT id_tarj, esta_tarj, tipo_tarj, cod_tarjeta FROM tarjeta"
                );
            }
            result = db.ExecuteQuery(psSelectAll);
            while (result.next()) {
                Tarjeta tarjeta = new Tarjeta();
                tarjeta.setId_tarj(result.getInt("id_tarj"));
                tarjeta.setEsta_tarj(result.getString("esta_tarj"));
                tarjeta.setTipo_tarj(result.getString("tipo_tarj"));
                tarjeta.setCodTarjeta(result.getString("cod_tarjeta"));

                listaTarjeta.add(tarjeta);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectAll != null) {
                try {
                    psSelectAll.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaTarjeta;
    }
    
    public Tarjeta get(String cod) {
        Tarjeta tarjeta = new Tarjeta();
        ResultSet result = null;
        PreparedStatement psSelect = null;
        try {
            if (psSelect == null) {
                psSelect = db.PreparedQuery(
                        "select id_tarj, esta_tarj, tipo_tarj, cod_tarjeta from tarjeta where cod_tarjeta = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(cod);
            result = db.ExecuteQuery(psSelect, inputs);
            while (result.next()) {
                tarjeta.setId_tarj(result.getInt("id_tarj"));
                tarjeta.setEsta_tarj(result.getString("esta_tarj"));
                tarjeta.setTipo_tarj(result.getString("tipo_tarj"));
                tarjeta.setCodTarjeta(result.getString("cod_tarjeta"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelect != null) {
                try {
                    psSelect.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return tarjeta;
    }
    
    public long insert(Tarjeta tarjeta) {
        PreparedStatement psInsert = null;
        long result;
        try {

            String columns = "esta_tarj, tipo_tarj, cod_tarjeta";
            String values = "?,?,?";
            if (psInsert == null) {
                psInsert = db.PreparedUpdate(
                        "INSERT INTO tarjeta(" + columns + ") VALUES(" + values + ")",
                        "id_tarj"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();

            if (tarjeta.getEsta_tarj() != null) {
                inputs.add(tarjeta.getEsta_tarj());
            } else {
                inputs.add(null);
            }
            if (tarjeta.getTipo_tarj() != null) {
                inputs.add(tarjeta.getTipo_tarj());
            } else {
                inputs.add(null);
            }
            if (tarjeta.getCodTarjeta() != null) {
                inputs.add(tarjeta.getCodTarjeta());
            } else {
                inputs.add(null);
            }

            result = db.ExecuteUpdate(psInsert, inputs);

        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar inserción.", e);
        } finally {
            if (psInsert != null) {
                try {
                    psInsert.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }
    
    public long update(Tarjeta tarjeta) {
        PreparedStatement psUpdate = null;
        long result;
        try {
            String columns = "";
            ArrayList<Object> inputs = new ArrayList<Object>();
            
            if (tarjeta.getEsta_tarj() != null) {
                columns += ",esta_tarj=?";
                inputs.add(tarjeta.getEsta_tarj());
            }
            if (tarjeta.getTipo_tarj() != null) {
                columns += ",tipo_tarj=?";
                inputs.add(tarjeta.getTipo_tarj());
            }
            if (tarjeta.getCodTarjeta() != null) {
                columns += ",cod_tarjeta=?";
                inputs.add(tarjeta.getCodTarjeta());
            }

            inputs.add(tarjeta.getId_tarj());
            columns = columns.substring(1);

            psUpdate = db.PreparedUpdate(
                    "UPDATE tarjeta SET " + columns + " WHERE id_tarj=?"
            );
            result = db.ExecuteUpdate(psUpdate, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar actualización.", e);
        } finally {
            if (psUpdate != null) {
                try {
                    psUpdate.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }
    
    public long delete(int idtarjeta) {
        PreparedStatement psDelete = null;
        long result;
        try {
            if (psDelete == null) {
                psDelete = db.PreparedUpdate(
                        "DELETE FROM tarjeta "
                        + "WHERE id_tarj=?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(idtarjeta);
            result = db.ExecuteUpdate(psDelete, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar borrado.", e);
        } finally {
            if (psDelete != null) {
                try {
                    psDelete.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) throws IOException {
        App App = new App();
        try {
            App.OpenConnection();
            // Se prueban los metodos del CRUD
        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            App.CloseConnection();
        }
    }
}
