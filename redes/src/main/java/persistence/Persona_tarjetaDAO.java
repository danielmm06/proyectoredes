/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import conexion.App;
import conexion.DataBase;
import entity.Persona;
import entity.Persona_tarjeta;
import entity.Tarjeta;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class Persona_tarjetaDAO {
    private DataBase db;

    public Persona_tarjetaDAO() {
        if (App.DB != null) {
            this.db = App.DB;
        } else {
            throw new RuntimeException("Error: No se ha inicializado la conexión.");
        }
    }
    
    public ArrayList<Persona_tarjeta> getAll() {
        ArrayList<Persona_tarjeta> listaPersona_tarjeta = new ArrayList<Persona_tarjeta>();
        ResultSet result = null;
        PreparedStatement psSelectAll = null;
        try {
            if (psSelectAll == null) {
                psSelectAll = db.PreparedQuery("select fech_asig, fech_devo, id_pers, id_tarj from persona_tarjeta");
            }
            result = db.ExecuteQuery(psSelectAll);
            while (result.next()) {
                Persona_tarjeta persona_tarjeta = new Persona_tarjeta();
                persona_tarjeta.setFech_asig(result.getString("fech_asig"));
                persona_tarjeta.setFech_devo(result.getString("fech_devo"));
                
                Persona persona = new Persona();
                persona.setId_pers(result.getInt("id_pers"));
                persona_tarjeta.setPers(persona);
                
                Tarjeta tarjeta = new Tarjeta();
                tarjeta.setId_tarj(result.getInt("id_tarj"));
                persona_tarjeta.setTarj(tarjeta);

                listaPersona_tarjeta.add(persona_tarjeta);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectAll != null) {
                try {
                    psSelectAll.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaPersona_tarjeta;
    }

    public Persona_tarjeta get(int id_pers, int id_tarj) {
        Persona_tarjeta persona_tarjeta = new Persona_tarjeta();
        ResultSet result = null;
        PreparedStatement psSelect = null;
        try {
            if (psSelect == null) {
                psSelect = db.PreparedQuery(
                        "select fech_asig, fech_devo, id_pers, id_tarj from persona_tarjeta where id_pers = ? and id_tarj = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(id_pers);
            inputs.add(id_tarj);
            result = db.ExecuteQuery(psSelect, inputs);
            while (result.next()) {
                persona_tarjeta.setFech_asig(result.getString("fech_asig"));
                persona_tarjeta.setFech_devo(result.getString("fech_devo"));
                
                Persona persona = new Persona();
                persona.setId_pers(result.getInt("id_pers"));
                persona_tarjeta.setPers(persona);
                
                Tarjeta tarjeta = new Tarjeta();
                tarjeta.setId_tarj(result.getInt("id_tarj"));
                persona_tarjeta.setTarj(tarjeta);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelect != null) {
                try {
                    psSelect.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return persona_tarjeta;
    }

    public long insert(Persona_tarjeta persona_tarjeta) {
        PreparedStatement psInsert = null;
        long result;
        try {

            String columns = "fech_asig, fech_devo, id_pers, id_tarj";
            String values = "?,?,?,?";
            if (psInsert == null) {
                psInsert = db.PreparedUpdate(
                        "INSERT INTO persona_tarjeta(" + columns + ") VALUES(" + values + ")"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();

            if (persona_tarjeta.getFech_asig()!= null) {
                inputs.add(persona_tarjeta.getFech_asig());
            }
            if (persona_tarjeta.getFech_devo() != null) {
                inputs.add(persona_tarjeta.getFech_devo());
            }
            if (persona_tarjeta.getPers() != null) {
                inputs.add(persona_tarjeta.getPers().getId_pers());
            } else {
                inputs.add(null);
            }
            if (persona_tarjeta.getTarj() != null) {
                inputs.add(persona_tarjeta.getTarj().getId_tarj());
            } else {
                inputs.add(null);
            }

            result = db.ExecuteUpdate(psInsert, inputs);

        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar inserción.", e);
        } finally {
            if (psInsert != null) {
                try {
                    psInsert.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long update(Persona_tarjeta persona_tarjeta) {
        PreparedStatement psUpdate = null;
        long result;
        try {
            String columns = "";
            ArrayList<Object> inputs = new ArrayList<Object>();
            
            if (persona_tarjeta.getFech_asig() != null) {
                columns += ",fech_asig=?";
                inputs.add(persona_tarjeta.getFech_asig());
            }
            if (persona_tarjeta.getFech_devo() != null) {
                columns += ",fech_devo=?";
                inputs.add(persona_tarjeta.getFech_devo());
            }
            if (persona_tarjeta.getPers() != null) {
                columns += ",id_pers=?";
                inputs.add(persona_tarjeta.getPers().getId_pers());
            }
            if (persona_tarjeta.getTarj() != null) {
                columns += ",id_tarj=?";
                inputs.add(persona_tarjeta.getTarj().getId_tarj());
            }

            inputs.add(persona_tarjeta.getPers().getId_pers());
            inputs.add(persona_tarjeta.getTarj().getId_tarj());
            columns = columns.substring(1);

            psUpdate = db.PreparedUpdate(
                    "UPDATE persona_tarjeta SET " + columns + " WHERE id_pers=? and id_tarj=?"
            );
            result = db.ExecuteUpdate(psUpdate, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar actualización.", e);
        } finally {
            if (psUpdate != null) {
                try {
                    psUpdate.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long delete(int id_pers, int id_tarj) {
        PreparedStatement psDelete = null;
        long result;
        try {
            if (psDelete == null) {
                psDelete = db.PreparedUpdate(
                        "DELETE FROM persona_tarjeta "
                        + "WHERE id_pers=? and id_tarj=?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(id_pers);
            inputs.add(id_tarj);
            result = db.ExecuteUpdate(psDelete, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar borrado.", e);
        } finally {
            if (psDelete != null) {
                try {
                    psDelete.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) throws IOException {
        App App = new App();
        try {
            App.OpenConnection();
            // Se prueban los metodos del CRUD
        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            App.CloseConnection();
        }
    }
}
