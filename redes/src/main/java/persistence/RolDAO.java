/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import conexion.App;
import conexion.DataBase;
import entity.Rol;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class RolDAO {
    private DataBase db;

    public RolDAO() {
        if (App.DB != null) {
            this.db = App.DB;
        } else {
            throw new RuntimeException("Error: No se ha inicializado la conexión.");
        }
    }
    
    public ArrayList<Rol> getAll() {
        ArrayList<Rol> listaRol = new ArrayList<Rol>();
        ResultSet result = null;
        PreparedStatement psSelectAll = null;
        try {
            if (psSelectAll == null) {
                psSelectAll = db.PreparedQuery("select id_rol, nomb_rol from rol");
            }
            result = db.ExecuteQuery(psSelectAll);
            while (result.next()) {
                Rol rol = new Rol();
                rol.setId_rol(result.getInt("id_rol"));
                rol.setNomb_rol(result.getString("nomb_rol"));

                listaRol.add(rol);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectAll != null) {
                try {
                    psSelectAll.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaRol;
    }
    
    public Rol get(int idRol) {
        Rol rol = new Rol();
        ResultSet result = null;
        PreparedStatement psSelect = null;
        try {
            if (psSelect == null) {
                psSelect = db.PreparedQuery(
                        "select id_rol, nomb_rol from rol where id_rol = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(idRol);
            result = db.ExecuteQuery(psSelect, inputs);
            while (result.next()) {
                rol.setId_rol(result.getInt("id_rol"));
                rol.setNomb_rol(result.getString("nomb_rol"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelect != null) {
                try {
                    psSelect.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return rol;
    }
    
    public long insert(Rol rol) {
        PreparedStatement psInsert = null;
        long result;
        try {

            String columns = "nomb_rol";
            String values = "?";
            if (psInsert == null) {
                psInsert = db.PreparedUpdate(
                        "INSERT INTO rol(" + columns + ") VALUES(" + values + ")",
                        "id_rol"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();

            if (rol.getNomb_rol() != null) {
                inputs.add(rol.getNomb_rol());
            } else {
                inputs.add(null);
            }

            result = db.ExecuteUpdate(psInsert, inputs);

        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar inserción.", e);
        } finally {
            if (psInsert != null) {
                try {
                    psInsert.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }
    
    public long update(Rol rol) {
        PreparedStatement psUpdate = null;
        long result;
        try {
            String columns = "";
            ArrayList<Object> inputs = new ArrayList<Object>();

            if (rol.getNomb_rol() != null) {
                columns += ",nomb_rol=?";
                inputs.add(rol.getNomb_rol());
            }

            inputs.add(rol.getId_rol());
            columns = columns.substring(1);

            psUpdate = db.PreparedUpdate(
                    "UPDATE rol SET " + columns + " WHERE id_rol=?"
            );
            result = db.ExecuteUpdate(psUpdate, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar actualización.", e);
        } finally {
            if (psUpdate != null) {
                try {
                    psUpdate.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }
    
    public long delete(int idRol) {
        PreparedStatement psDelete = null;
        long result;
        try {
            if (psDelete == null) {
                psDelete = db.PreparedUpdate(
                        "DELETE FROM rol "
                        + "WHERE id_rol=?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(idRol);
            result = db.ExecuteUpdate(psDelete, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar borrado.", e);
        } finally {
            if (psDelete != null) {
                try {
                    psDelete.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }
    
    public Rol getNombre(String nombre) {
        Rol rol = new Rol();
        ResultSet result = null;
        PreparedStatement psSelectN = null;
        try {
            if (psSelectN == null) {
                psSelectN = db.PreparedQuery(
                        "select id_rol, nomb_rol from rol where nomb_rol = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(nombre);
            result = db.ExecuteQuery(psSelectN, inputs);
            while (result.next()) {
                rol.setId_rol(result.getInt("id_rol"));
                rol.setNomb_rol(result.getString("nomb_rol"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectN != null) {
                try {
                    psSelectN.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return rol;
    }   

    public static void main(String[] args) throws IOException {
        App App = new App();
        try {
            App.OpenConnection();
            // Se prueban los metodos del CRUD
//              System.out.println("--> GET ALL");
//              ArrayList<Rol> listaDescuentos = App.RolDAO.getAll();
//              for (Rol listaDescuento : listaDescuentos) {
//                  System.out.println(listaDescuento.getId_rol()+" - "+listaDescuento.getNomb_rol());
//              }

//              System.out.println("--> GET");
//              Rol rol = App.RolDAO.getNombre("estudiante");
//              System.out.println(rol.getId_rol()+" - "+rol.getNomb_rol());
//
//              System.out.println("--> INSERT");
//              Rol rol1 = new Rol();
//              rol1.setNomb_rol("Estudiante");
//              System.out.println(App.RolDAO.insert(rol1));
//
//              System.out.println("--> UPDATE");
//              Rol rol2 = new Rol();
//              rol2.setId_rol(3);
//              rol2.setNomb_rol("Docente");
//              System.out.println(App.RolDAO.update(rol2));
//            
//                System.out.println("DELETE");
//                System.out.println(App.RolDAO.delete(3));
        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            App.CloseConnection();
        }
    }
}
