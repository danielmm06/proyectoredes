/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import conexion.App;
import conexion.DataBase;
import entity.Lector;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class LectorDAO {

    private DataBase db;

    public LectorDAO() {
        if (App.DB != null) {
            this.db = App.DB;
        } else {
            throw new RuntimeException("Error: No se ha inicializado la conexión.");
        }
    }

    public ArrayList<Lector> getAll() {
        ArrayList<Lector> listaLector = new ArrayList<Lector>();
        ResultSet result = null;
        PreparedStatement psSelectAll = null;
        try {
            if (psSelectAll == null) {
                psSelectAll = db.PreparedQuery("select id_lect, ubic_lect from lector");
            }
            result = db.ExecuteQuery(psSelectAll);
            while (result.next()) {
                Lector lector = new Lector();
                lector.setId_lect(result.getInt("id_lect"));
                lector.setUbic_lect(result.getString("ubic_lect"));

                listaLector.add(lector);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectAll != null) {
                try {
                    psSelectAll.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaLector;
    }

    public Lector get(int idLector) {
        Lector lector = new Lector();
        ResultSet result = null;
        PreparedStatement psSelect = null;
        try {
            if (psSelect == null) {
                psSelect = db.PreparedQuery(
                        "select id_lect, ubic_lect from lector where id_lect = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(idLector);
            result = db.ExecuteQuery(psSelect, inputs);
            while (result.next()) {
                lector.setId_lect(result.getInt("id_lect"));
                lector.setUbic_lect(result.getString("ubic_lect"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelect != null) {
                try {
                    psSelect.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return lector;
    }

    public long insert(Lector lector) {
        PreparedStatement psInsert = null;
        long result;
        try {

            String columns = "ubic_lect";
            String values = "?";
            if (psInsert == null) {
                psInsert = db.PreparedUpdate(
                        "INSERT INTO lector(" + columns + ") VALUES(" + values + ")",
                        "id_lect"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();

            if (lector.getUbic_lect() != null) {
                inputs.add(lector.getUbic_lect());
            } else {
                inputs.add(null);
            }

            result = db.ExecuteUpdate(psInsert, inputs);

        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar inserción.", e);
        } finally {
            if (psInsert != null) {
                try {
                    psInsert.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long update(Lector lector) {
        PreparedStatement psUpdate = null;
        long result;
        try {
            String columns = "";
            ArrayList<Object> inputs = new ArrayList<Object>();

            if (lector.getUbic_lect() != null) {
                columns += ",ubic_lect=?";
                inputs.add(lector.getUbic_lect());
            }

            inputs.add(lector.getId_lect());
            columns = columns.substring(1);

            psUpdate = db.PreparedUpdate(
                    "UPDATE lector SET " + columns + " WHERE id_lect=?"
            );
            result = db.ExecuteUpdate(psUpdate, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar actualización.", e);
        } finally {
            if (psUpdate != null) {
                try {
                    psUpdate.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long delete(int idLector) {
        PreparedStatement psDelete = null;
        long result;
        try {
            if (psDelete == null) {
                psDelete = db.PreparedUpdate(
                        "DELETE FROM lector "
                        + "WHERE id_lect=?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(idLector);
            result = db.ExecuteUpdate(psDelete, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar borrado.", e);
        } finally {
            if (psDelete != null) {
                try {
                    psDelete.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }
    
    public Lector getNombre(String nombre) {
        Lector lector = new Lector();
        ResultSet result = null;
        PreparedStatement psSelect = null;
        try {
            if (psSelect == null) {
                psSelect = db.PreparedQuery(
                        "select id_lect, ubic_lect from lector where ubic_lect = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(nombre);
            result = db.ExecuteQuery(psSelect, inputs);
            while (result.next()) {
                lector.setId_lect(result.getInt("id_lect"));
                lector.setUbic_lect(result.getString("ubic_lect"));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelect != null) {
                try {
                    psSelect.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return lector;
    }

    public static void main(String[] args) throws IOException {
        App App = new App();
        try {
            App.OpenConnection();
            // Se prueban los metodos del CRUD

//            System.out.println("--> GET ALL");
//            ArrayList<Lector> listaDescuentos = App.LectorDAO.getAll();
//            for (Lector listaDescuento : listaDescuentos) {
//                System.out.println(listaDescuento.getId_lect()+" - "+listaDescuento.getUbic_lect());
//            }

//            System.out.println("--> GET");
//            Lector lector = App.LectorDAO.get(1);
//            System.out.println(lector.getId_lect()+" - "+lector.getUbic_lect());

//            System.out.println("--> INSERT");
//            Lector lector = new Lector();
//            lector.setUbic_lect("Sala A");
//            System.out.println(App.LectorDAO.insert(lector));

//            System.out.println("--> UPDATE");
//            Lector lector = new Lector();
//            lector.setId_lect(3);
//            lector.setUbic_lect("Sala Z");
//            System.out.println(App.LectorDAO.update(lector));
            
//              System.out.println("DELETE");
//              System.out.println(App.LectorDAO.delete(3));
              
        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            App.CloseConnection();
        }
    }
}
