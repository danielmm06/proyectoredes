/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import conexion.App;
import conexion.DataBase;
import entity.Bic;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class BicDAO {
    private DataBase db;

    public BicDAO() {
        if (App.DB != null) {
            this.db = App.DB;
        } else {
            throw new RuntimeException("Error: No se ha inicializado la conexión.");
        }
    }
    
    public ArrayList<Bic> getAll() {
        ArrayList<Bic> listaBic = new ArrayList<Bic>();
        ResultSet result = null;
        PreparedStatement psSelectAll = null;
        try {
            if (psSelectAll == null) {
                psSelectAll = db.PreparedQuery("select codigo, fecha, usuario, nomb_table, oper, valor_v, valor_n from bic;");
            }
            result = db.ExecuteQuery(psSelectAll);
            while (result.next()) {
                Bic bic = new Bic();
                bic.setCodigo(result.getInt("codigo"));
                bic.setFecha(result.getString("fecha"));
                bic.setUsuario(result.getString("usuario"));
                bic.setNomb_table(result.getString("nomb_table"));
                bic.setOper(result.getString("oper"));
                bic.setValor_v(result.getString("valor_v"));
                bic.setValor_n(result.getString("valor_n"));
                
                listaBic.add(bic);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectAll != null) {
                try {
                    psSelectAll.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaBic;
    }
   

    public static void main(String[] args) throws IOException {
        App App = new App();
        try {
            App.OpenConnection();
            // Se prueban los metodos del CRUD
            
//            System.out.println("--> GET ALL");
//            ArrayList<Bic> listaBic = App.BicDAO.getAll();
//            for (Bic bic : listaBic) {
//                System.out.println(bic.getCodigo()+" - "+bic.getFecha()+" - "+bic.getUsuario()+" - "+bic.getNomb_table()+" - "+bic.getOper()+" - "+bic.getValor_n()+" - "+bic.getValor_v());
//            }

        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            App.CloseConnection();
        }
    }
}
