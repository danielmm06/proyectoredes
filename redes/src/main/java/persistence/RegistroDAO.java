/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import conexion.App;
import conexion.DataBase;
import entity.Lector;
import entity.Persona;
import entity.Registro;
import entity.Tarjeta;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class RegistroDAO {

    private DataBase db;

    public RegistroDAO() {
        if (App.DB != null) {
            this.db = App.DB;
        } else {
            throw new RuntimeException("Error: No se ha inicializado la conexión.");
        }
    }

    public ArrayList<Registro> getAll() {
        ArrayList<Registro> listaRegistro = new ArrayList<Registro>();
        ResultSet result = null;
        PreparedStatement psSelectAll = null;
        try {
            if (psSelectAll == null) {
                psSelectAll = db.PreparedQuery("select id_regi, fech_regi, hora_regi, id_lect, id_tarj from registro");
            }
            result = db.ExecuteQuery(psSelectAll);
            while (result.next()) {
                Registro registro = new Registro();
                registro.setId_regi(result.getInt("id_regi"));
                registro.setFech_regi(result.getString("fech_regi"));
                registro.setHora_regi(result.getString("hora_regi"));

                Lector lector = new Lector();
                lector.setId_lect(result.getInt("id_lect"));
                registro.setLector(lector);

                Tarjeta tarjeta = new Tarjeta();
                tarjeta.setId_tarj(result.getInt("id_tarj"));
                registro.setTarjeta(tarjeta);

                listaRegistro.add(registro);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectAll != null) {
                try {
                    psSelectAll.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaRegistro;
    }

    public Registro get(int idRegistro) {
        Registro registro = new Registro();
        ResultSet result = null;
        PreparedStatement psSelect = null;
        try {
            if (psSelect == null) {
                psSelect = db.PreparedQuery(
                        "select id_regi, fech_regi, hora_regi, id_lect, id_tarj from registro where id_regi = ?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(idRegistro);
            result = db.ExecuteQuery(psSelect, inputs);
            while (result.next()) {
                registro.setId_regi(result.getInt("id_regi"));
                registro.setFech_regi(result.getString("fech_regi"));
                registro.setHora_regi(result.getString("hora_regi"));

                Lector lector = new Lector();
                lector.setId_lect(result.getInt("id_lect"));
                registro.setLector(lector);

                Tarjeta tarjeta = new Tarjeta();
                tarjeta.setId_tarj(result.getInt("id_tarj"));
                registro.setTarjeta(tarjeta);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelect != null) {
                try {
                    psSelect.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return registro;
    }

    public long insert(Registro registro) {
        PreparedStatement psInsert = null;
        long result;
        try {

            String columns = "fech_regi, hora_regi, id_lect, id_tarj";
            String values = "?,?,?,?";
            if (psInsert == null) {
                psInsert = db.PreparedUpdate(
                        "INSERT INTO registro(" + columns + ") VALUES(" + values + ")",
                        "id_regi"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();

            if (registro.getFech_regi() != null) {
                inputs.add(registro.getFech_regi());
            }
            if (registro.getHora_regi() != null) {
                inputs.add(registro.getHora_regi());
            }
            if (registro.getLector() != null) {
                inputs.add(registro.getLector().getId_lect());
            }
            if (registro.getTarjeta() != null) {
                inputs.add(registro.getTarjeta().getId_tarj());
            }

            result = db.ExecuteUpdate(psInsert, inputs);

        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar inserción.", e);
        } finally {
            if (psInsert != null) {
                try {
                    psInsert.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long update(Registro registro) {
        PreparedStatement psUpdate = null;
        long result;
        try {
            String columns = "";
            ArrayList<Object> inputs = new ArrayList<Object>();

            if (registro.getFech_regi() != null) {
                columns += ",fech_regi=?";
                inputs.add(registro.getFech_regi());
            }
            if (registro.getHora_regi() != null) {
                columns += ",hora_regi=?";
                inputs.add(registro.getHora_regi());
            }
            if (registro.getLector() != null) {
                columns += ",id_lect=?";
                inputs.add(registro.getLector().getId_lect());
            }
            if (registro.getTarjeta() != null) {
                columns += ",id_tarj=?";
                inputs.add(registro.getTarjeta().getId_tarj());
            }

            inputs.add(registro.getId_regi());
            columns = columns.substring(1);

            psUpdate = db.PreparedUpdate(
                    "UPDATE registro SET " + columns + " WHERE id_regi=?"
            );
            result = db.ExecuteUpdate(psUpdate, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar actualización.", e);
        } finally {
            if (psUpdate != null) {
                try {
                    psUpdate.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public long delete(int idRegistro) {
        PreparedStatement psDelete = null;
        long result;
        try {
            if (psDelete == null) {
                psDelete = db.PreparedUpdate(
                        "DELETE FROM registro "
                        + "WHERE id_regi=?"
                );
            }
            ArrayList<Object> inputs = new ArrayList<Object>();
            inputs.add(idRegistro);
            result = db.ExecuteUpdate(psDelete, inputs);
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar borrado.", e);
        } finally {
            if (psDelete != null) {
                try {
                    psDelete.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return result;
    }

    public ArrayList<Registro> getAllRegistros() {
        ArrayList<Registro> listaRegistros = new ArrayList<Registro>();
        ResultSet result = null;
        PreparedStatement psSelectAllRegistros = null;
        try {
            if (psSelectAllRegistros == null) {
                psSelectAllRegistros = db.PreparedQuery("select resg.fech_regi, resg.hora_regi, lec.ubic_lect, tar.cod_tarjeta, CONCAT(pers.nomb_pers,' ',pers.apell_pers) As Nombre "
                        + "from registro resg, lector lec, tarjeta tar, persona_tarjeta pt, persona pers "
                        + "where resg.id_lect = lec.id_lect "
                        + "and resg.id_tarj = tar.id_tarj "
                        + "and resg.id_tarj = pt.id_tarj "
                        + "and pt.id_pers = pers.id_pers");
            }
            result = db.ExecuteQuery(psSelectAllRegistros);
            while (result.next()) {
                Registro registro = new Registro();
                registro.setFech_regi(result.getString("fech_regi"));
                registro.setHora_regi(result.getString("hora_regi"));

                Lector lector = new Lector();
                lector.setUbic_lect(result.getString("ubic_lect"));
                registro.setLector(lector);

                Tarjeta tarjeta = new Tarjeta();
                tarjeta.setCodTarjeta(result.getString("cod_tarjeta"));
                registro.setTarjeta(tarjeta);

                Persona persona = new Persona();
                persona.setNomb_pers(result.getString("Nombre"));
                registro.setPersona(persona);

                listaRegistros.add(registro);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectAllRegistros != null) {
                try {
                    psSelectAllRegistros.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaRegistros;
    }

    public ArrayList<Registro> getByMultiple(String fini, String ffin, String hini, String hfin) {
//        System.out.println("fini "+fini+" ffin "+ffin+" hini "+hini+" hfin "+hfin);
        ArrayList<Registro> listaRegistros = new ArrayList<Registro>();
        ResultSet result = null;
        PreparedStatement psSelectByMultiple = null;
        try {
            ArrayList<Object> inputs = new ArrayList<Object>();
            
            String consulta = "";
            String select = "";
            String from = "";
            String where = "";

            select = " SELECT "
                    + " resg.fech_regi, resg.hora_regi, lec.ubic_lect, tar.cod_tarjeta, CONCAT(pers.nomb_pers,' ',pers.apell_pers) As Nombre ";
            from = " FROM "
                    + "registro resg, lector lec, tarjeta tar, persona_tarjeta pt, persona pers ";
            where = " WHERE "
                    + " resg.id_lect = lec.id_lect "
                    + " AND resg.id_tarj = tar.id_tarj "
                    + " AND resg.id_tarj = pt.id_tarj "
                    + " AND pt.id_pers = pers.id_pers  ";

            if ((hini != "") && (hfin != "")) {
                where += " AND resg.hora_regi >= ? "
                       + " AND resg.hora_regi <= ? ";
                inputs.add(hini);
                inputs.add(hfin);
            }
            if ((hini != "") && (hfin == "")) {
                where += " AND resg.hora_regi >= ? ";
                 inputs.add(hini);
            }
            if ((hini == "") && (hfin != "")) {
                where += " AND resg.hora_regi <= ? ";
                inputs.add(hfin);
            }
            
            
            if ((fini != "") && (ffin != "")) {
                where += " AND resg.fech_regi >= ? "
                       + " AND resg.fech_regi <= ? ";
                inputs.add(fini);
                inputs.add(ffin);
            }
            if ((fini != "") && (ffin == "")) {
                where += " AND resg.fech_regi >= ? ";
                 inputs.add(fini);
            }
            if ((fini == "") && (ffin != "")) {
                where += " AND resg.fech_regi <= ? ";
                inputs.add(ffin);
            }

            consulta = select + from + where;
//            System.out.println("-->> "+consulta);
//            for (Object input : inputs) {
//                System.out.println(input);
//            }
            if (psSelectByMultiple == null) {
                psSelectByMultiple = db.PreparedQuery(consulta);
            }
            result = db.ExecuteQuery(psSelectByMultiple, inputs);
            while (result.next()) {
                Registro registro = new Registro();
                registro.setFech_regi(result.getString("fech_regi"));
                registro.setHora_regi(result.getString("hora_regi"));

                Lector lector = new Lector();
                lector.setUbic_lect(result.getString("ubic_lect"));
                registro.setLector(lector);

                Tarjeta tarjeta = new Tarjeta();
                tarjeta.setCodTarjeta(result.getString("cod_tarjeta"));
                registro.setTarjeta(tarjeta);

                Persona persona = new Persona();
                persona.setNomb_pers(result.getString("Nombre"));
                registro.setPersona(persona);

                listaRegistros.add(registro);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error al ejecutar consulta.", e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el resultset", e);
                }
            }
            if (psSelectByMultiple != null) {
                try {
                    psSelectByMultiple.close();
                } catch (SQLException e) {
                    throw new RuntimeException("Error al cerrar el preparedstatement", e);
                }
            }
        }
        return listaRegistros;
    }

    public static void main(String[] args) throws IOException {
        App App = new App();
        try {
            App.OpenConnection();
            // Se prueban los metodos del CRUD

//            System.out.println("--> GET ALL");
//            ArrayList<Registro> listaRegistro = App.RegistroDAO.getAll();
//            for (Registro registro : listaRegistro) {
//                System.out.println(registro.getId_regi()+" - "+registro.getFech_regi()+" - "+registro.getHora_regi()+" - "+registro.getLector().getId_lect()+" - "+registro.getTarjeta().getId_tarj());
//            }
//            System.out.println("--> GET");
//            Registro registro = App.RegistroDAO.get(1);
//            System.out.println(registro.getId_regi()+" - "+registro.getFech_regi()+" - "+registro.getHora_regi()+" - "+registro.getLector().getId_lect()+" - "+registro.getTarjeta().getId_tarj());
//            System.out.println("--> INSERT");
//            Registro registro = new Registro();
//            registro.setFech_regi("2020-02-20");
//            registro.setHora_regi("22:00:00");
//            Lector lector = new Lector();
//            lector.setId_lect(1);
//            registro.setLector(lector);
//            Tarjeta tarjeta = new Tarjeta();
//            tarjeta.setId_tarj(1);
//            registro.setTarjeta(tarjeta);
//            System.out.println(App.RegistroDAO.insert(registro));
//            System.out.println("--> UPDATE");
//            Registro registro = new Registro();
//            registro.setId_regi(3);
//            registro.setFech_regi("2020-02-21");
//            registro.setHora_regi("23:00:00");
//            Lector lector = new Lector();
//            lector.setId_lect(2);
//            registro.setLector(lector);
//            Tarjeta tarjeta = new Tarjeta();
//            tarjeta.setId_tarj(1);
//            registro.setTarjeta(tarjeta);
//            System.out.println(App.RegistroDAO.update(registro));
//              System.out.println("DELETE");
//              System.out.println(App.RegistroDAO.delete(3));
//            System.out.println("--> GET ALL");
//            ArrayList<Registro> listaRegistro = App.RegistroDAO.getAllRegistros();
//            for (Registro registro : listaRegistro) {
//                System.out.println(registro.getFech_regi()+" - "+registro.getHora_regi()+" - "+registro.getLector().getUbic_lect()+" - "+registro.getTarjeta().getCodTarjeta()+" - "+registro.getPersona().getNomb_pers());
//            }
        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            App.CloseConnection();
        }
    }
}
