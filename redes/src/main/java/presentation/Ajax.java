/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import conexion.App;
import entity.Bic;
import entity.Lector;
import entity.Persona;
import entity.Registro;
import entity.Rol;
import entity.Tarjeta;
import entity.Usuario;

/**
 *
 * @author Daniel
 */
@WebServlet(name = "Ajax", urlPatterns = {"/Ajax"})
public class Ajax extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public Ajax() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/vnd.wap.xhtml+xml");
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        processAjax(request, response.getWriter(), request.getParameter("value"), response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/vnd.wap.xhtml+xml");
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");
        processAjax(request, response.getWriter(), request.getParameter("value"), response);
    }

    protected void processAjax(HttpServletRequest request, PrintWriter out, String value, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
//        System.out.println("value "+value);

        if (value.equalsIgnoreCase("ListRegistros")) {
            try {
                App.OpenConnection();

                String fini = request.getParameter("fini");
                String ffin = request.getParameter("ffin");
                String hini = request.getParameter("hini");
                String hfin = request.getParameter("hfin");

                ArrayList<Registro> ListRegistro = App.RegistroDAO.getByMultiple(fini, ffin, hini, hfin);

                // Manejo_de_JSON
                JsonArray array = new JsonArray();
                for (Registro registro : ListRegistro) {
                    JsonObject item = new JsonObject();

                    item.addProperty("fecha", registro.getFech_regi());
                    item.addProperty("hora", registro.getHora_regi());
                    item.addProperty("lector", registro.getLector().getUbic_lect());
                    item.addProperty("cod_terjeta", registro.getTarjeta().getCodTarjeta());
                    item.addProperty("nombre", registro.getPersona().getNomb_pers());
                    array.add(item);
                }
                out.print(array);
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("ListaBic")) {
            try {
                App.OpenConnection();

                ArrayList<Bic> ListBic = App.BicDAO.getAll();
                // Manejo_de_JSON
                JsonArray array = new JsonArray();
                for (Bic bic : ListBic) {
                    JsonObject item = new JsonObject();
                    item.addProperty("fecha", bic.getFecha());
                    item.addProperty("usuario", bic.getUsuario());
                    item.addProperty("nomb_table", bic.getNomb_table());
                    item.addProperty("oper", bic.getOper());
                    item.addProperty("valor_v", bic.getValor_v());
                    item.addProperty("valor_n", bic.getValor_n());
                    array.add(item);
                }
                out.print(array);
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("ListaLec")) {
            try {
                App.OpenConnection();

                ArrayList<Lector> ListBic = App.LectorDAO.getAll();
                // Manejo_de_JSON
                JsonArray array = new JsonArray();
                for (Lector lector : ListBic) {
                    JsonObject item = new JsonObject();
                    item.addProperty("id", lector.getId_lect());
                    item.addProperty("ubicacion", lector.getUbic_lect());
                    array.add(item);
                }
                out.print(array);
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("crearLector")) {
            try {
                App.OpenConnection();
                String crear = request.getParameter("content");
                Lector lector = new Lector();
                lector.setUbic_lect(crear);
                out.print(App.LectorDAO.insert(lector));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("updateLector")) {
            try {
                App.OpenConnection();
                String[] update = (request.getParameter("content")).split(",");
                Lector lector = App.LectorDAO.getNombre(update[0]);
                lector.setUbic_lect(update[1]);
                out.print(App.LectorDAO.update(lector));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("eliminarLector")) {
            try {
                App.OpenConnection();
                String eliminar = request.getParameter("content");
                Lector lector = App.LectorDAO.getNombre(eliminar);
                System.out.println(lector.getId_lect());
                out.print(App.LectorDAO.delete(lector.getId_lect()));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("ListaUsuario")) {
            try {
                App.OpenConnection();

                ArrayList<Usuario> ListUsu = App.UsuarioDAO.getAll();
                // Manejo_de_JSON
                JsonArray array = new JsonArray();
                for (Usuario usuario : ListUsu) {
                    JsonObject item = new JsonObject();
                    item.addProperty("nomb_user", usuario.getNomb_user());
                    Persona persona = App.PersonaDAO.get(usuario.getPersona().getId_pers());
                    item.addProperty("doc", persona.getNum_documento());
                    item.addProperty("pers", persona.getNomb_pers() + " " + persona.getApell_pers());
                    array.add(item);
                }
                out.print(array);
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("crearUsuario")) {
            try {
                App.OpenConnection();
                String[] datos = (request.getParameter("content")).split(",");
                String user = datos[0];
                String pass = datos[1];
                String doc = datos[2];
                Persona persona = App.PersonaDAO.getDoc(doc);
                Usuario usuario = new Usuario();
                usuario.setNomb_user(user);
                usuario.setPass_user(pass);
                usuario.setPersona(persona);
//                System.out.println(user+" "+pass+" "+doc);
                out.print(App.UsuarioDAO.insert(usuario));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("updateUser")) {
            try {
                App.OpenConnection();
                String[] datos = (request.getParameter("content")).split(",");
                String user = datos[0];
                String pass = datos[1];

                Usuario usuario = new Usuario();
                usuario.setNomb_user(user);
                usuario.setPass_user(pass);
                System.out.println(user + " " + pass);
                out.print(App.UsuarioDAO.update(usuario));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("eliminaruser")) {
            try {
                App.OpenConnection();
                String eliminar = request.getParameter("content");
                out.print(App.UsuarioDAO.delete(eliminar));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("ListaRol")) {
            try {
                App.OpenConnection();

                ArrayList<Rol> ListRol = App.RolDAO.getAll();
                // Manejo_de_JSON
                JsonArray array = new JsonArray();
                for (Rol rol : ListRol) {
                    JsonObject item = new JsonObject();
                    item.addProperty("id", rol.getId_rol());
                    item.addProperty("rol", rol.getNomb_rol());
                    array.add(item);
                }
                out.print(array);
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("crearRol")) {
            try {
                App.OpenConnection();
                String datos = request.getParameter("content");

                Rol rol = new Rol();
                rol.setNomb_rol(datos);
                out.print(App.RolDAO.insert(rol));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("updateRol")) {
            try {
                App.OpenConnection();
                String[] datos = (request.getParameter("content")).split(",");
                String actual = datos[0];
                String nuevo = datos[1];

                Rol rol = App.RolDAO.getNombre(actual);
                out.print(App.RolDAO.update(rol));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("roldel")) {
            try {
                App.OpenConnection();
                String eliminar = request.getParameter("content");
                Rol rol = App.RolDAO.getNombre(eliminar);
                out.print(App.RolDAO.delete(rol.getId_rol()));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("ListaTarjeta")) {
            try {
                App.OpenConnection();

                ArrayList<Tarjeta> ListTarjeta = App.TarjetaDAO.getAll();
                // Manejo_de_JSON
                JsonArray array = new JsonArray();
                for (Tarjeta tarjeta : ListTarjeta) {
                    JsonObject item = new JsonObject();
                    item.addProperty("id", tarjeta.getId_tarj());
                    item.addProperty("cod", tarjeta.getCodTarjeta());
                    item.addProperty("tipo", tarjeta.getTipo_tarj());
                    item.addProperty("estado", tarjeta.getEsta_tarj());
                    array.add(item);
                }
                out.print(array);
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("crearTarjeta")) {
            try {
                App.OpenConnection();
                String[] datos = (request.getParameter("content")).split(",");
                String cod = datos[0];
                String tipo = datos[1];
                String estado = datos[2];

                Tarjeta tarjeta = new Tarjeta();
                tarjeta.setCodTarjeta(cod);
                tarjeta.setTipo_tarj(tipo);
                tarjeta.setEsta_tarj(estado);

                out.print(App.TarjetaDAO.insert(tarjeta));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("UpdateTarjeta")) {
            try {
                App.OpenConnection();
                String[] datos = (request.getParameter("content")).split(",");
                String cod = datos[0];
                String estado = datos[1];

                Tarjeta tarjeta = App.TarjetaDAO.get(cod);
                tarjeta.setEsta_tarj(estado);

                out.print(App.TarjetaDAO.update(tarjeta));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }

        if (value.equalsIgnoreCase("deleteTarjeta")) {
            try {
                App.OpenConnection();
                String cod = request.getParameter("content");

                Tarjeta tarjeta = App.TarjetaDAO.get(cod);
                out.print(App.TarjetaDAO.delete(tarjeta.getId_tarj()));
            } catch (Exception e) {
                throw new RuntimeException("Se ha generado un error inesperado", e);
            } finally {
                App.CloseConnection();
            }
        }
        
        if (value.equalsIgnoreCase("GenerarExcel")) {
        	String UPLOAD_DIRECTORY = "C:/Users/"+ System.getProperty("user.name") + "/Documents/reportesRedes"; // PRUEBAS
//        	String UPLOAD_DIRECTORY = "/home/Documentos/reportesRedes"; //SERVIDOR
			try {
				App.OpenConnection();
				File directorio = new File(UPLOAD_DIRECTORY);
				if (!directorio.exists()) {
					directorio.mkdir();
				}
				directorio = new File(UPLOAD_DIRECTORY +"/Reportes");
				if (!directorio.exists()) {
					directorio.mkdir();
				}
				String[] datos = request.getParameter("content").split("<_>");
				Path destino = Paths.get(UPLOAD_DIRECTORY + "/Reportes/Reporte.xlsx" );
				String URL = "reportesRedes/Reportes/Reporte.xlsx";
//				System.out.println("URL -- "+URL);
				Excel excel = new Excel(destino.toString());
				
				ArrayList<String> cabecera = new ArrayList<String>();
				String[] datosCabecera = datos[0].split(",");
				for (int i = 0; i < datosCabecera.length; i++) {
					cabecera.add(datosCabecera[i]);
				}
				excel.setCabecera("Reportes",cabecera);
				ArrayList<ArrayList<String>> registros = new ArrayList<ArrayList<String>>();
				String[] datosRegistro = datos[1].split("<->");
				for (int i = 0; i < datosRegistro.length; i++) {
					String[] datosRegistroTemp = datosRegistro[i].split("<,>");
					ArrayList<String> filas = new ArrayList<String>();
					for (int j = 0; j < datosRegistroTemp.length; j++) {
						switch (datosRegistroTemp[j]) {
						case "null":
							filas.add("");
							break;
						default:
							filas.add(datosRegistroTemp[j]);
							break;
						}
					}
					registros.add(filas);
				}
				excel.setReporte(registros);
				excel.close();
				out.print(URL);
			} catch (Exception e) {
				throw new RuntimeException("Se ha generado un error inesperado", e);
			} finally {
				App.CloseConnection();
			}
		}

    }
}
