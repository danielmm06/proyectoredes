/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import conexion.App;
import entity.Usuario;
import persistence.UsuarioDAO;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Daniel
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public Login() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {// Inicializa conexión con base de datos
            App.OpenConnection();

            boolean sesionValida = true;
            boolean permisoValido = true;
            // Acciones, Envío de parametros y Redirección
            if (sesionValida) {
                if (permisoValido) {
                    request.setCharacterEncoding("UTF-8");
                    request.setAttribute("title", App.nameProyect + " - Login");

                    HttpSession session = request.getSession();
                    session.invalidate();

                    getServletConfig().getServletContext().getRequestDispatcher("/views/login.jsp").forward(request, response);
                } else {
                    response.sendRedirect("Error?e=NotAuthorized");
                }
            } else {
                response.sendRedirect("Logout");
            }
        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            // Cierra conexión
            App.CloseConnection();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //Inicializa conexión con base de datos
            App.OpenConnection();
            request.setCharacterEncoding("UTF-8");

            //Recolección de parametros
            String nickname = request.getParameter("Usuario");
            String password = request.getParameter("Contrasena");

            //Ejecución de consultas
            UsuarioDAO usuarioDAO = new UsuarioDAO();
            Usuario usuario = usuarioDAO.get(nickname);

            //Procesamiento de la información
            boolean valido = App.AuthUser(usuario, password);
//                System.out.println("valido--->" + valido);

            if (valido == true) {
                HttpSession session = request.getSession(true);
                session.setAttribute("user", nickname);
                session.setAttribute("idPersona", usuario.getPersona().getId_pers());

                response.sendRedirect("Home");
            } else {
                response.sendRedirect("Login");
            }

        } catch (Exception e) {
            throw new RuntimeException("Se ha generado un error inesperado", e);
        } finally {
            //Cierra conexión 
            App.CloseConnection();
        }
    }

}