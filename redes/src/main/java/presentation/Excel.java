
package presentation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Daniel
 */
public class Excel {
	
	FileInputStream original;
	FileOutputStream nuevo;
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	XSSFCellStyle styleCabecera, styleCelda;
	int indexRow;
	
	public Excel(String urlSource, String urlDestination) {
		try {
			original = new FileInputStream(urlSource);
			nuevo = new FileOutputStream(urlDestination);
			workbook = new XSSFWorkbook (original);
			sheet = workbook.getSheetAt(0);
			sheet.setFitToPage(true);
			sheet.getPrintSetup().setFitWidth((short)1);
			sheet.getPrintSetup().setFitHeight((short)0);
		} catch (FileNotFoundException ex) {
        	ex.printStackTrace();
        } catch (IOException ex) {
        	ex.printStackTrace();
        }
	}
	
	public Excel(String urlDestination) {
		try {
			nuevo = new FileOutputStream(urlDestination);
			workbook = new XSSFWorkbook();
			
			styleCabecera = workbook.createCellStyle();
			styleCabecera.setBorderRight(BorderStyle.THIN);
			styleCabecera.setBorderLeft(BorderStyle.THIN);
			styleCabecera.setBorderBottom(BorderStyle.THIN);
			styleCabecera.setBorderTop(BorderStyle.THIN);
			styleCabecera.setAlignment(HorizontalAlignment.CENTER);
			styleCabecera.setFillForegroundColor(IndexedColors.DARK_RED.index);
			styleCabecera.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			Font letra = workbook.createFont();
			letra.setBold(true);
			letra.setColor(IndexedColors.WHITE.index);
			letra.setFontHeightInPoints((short)12);
			styleCabecera.setFont(letra);
			
			styleCelda = workbook.createCellStyle();
			styleCelda.setBorderRight(BorderStyle.THIN);
			styleCelda.setBorderLeft(BorderStyle.THIN);
			styleCelda.setBorderBottom(BorderStyle.THIN);
			styleCelda.setBorderTop(BorderStyle.THIN);
			styleCelda.setAlignment(HorizontalAlignment.CENTER);
		} catch (FileNotFoundException ex) {
        	ex.printStackTrace();
        }
	}

	public void replace(String clave, String valor){
		search:
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			XSSFRow row = sheet.getRow(i);
			if (row != null) {
				for (int j = 0; j < row.getLastCellNum(); j++) {
					XSSFCell cell = row.getCell(j);
					if (cell == null) {
						break;
					} else if (cell.toString().contains(clave)) {
						cell.setCellValue(valor);
						break search;
					}
				}
			}
		}
	}
	
	public void clean() {
		Pattern pat = Pattern.compile("^\\s*<\\d*>\\s*$");
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			XSSFRow row = sheet.getRow(i);
			if (row != null) {
				for (int j = 0; j < row.getLastCellNum(); j++) {
					XSSFCell cell = row.getCell(j);
					if (cell == null) {
						break;
					} else if (pat.matcher(cell.toString()).matches()) {
						cell.setCellValue("");
					}
				}
			}
		}
	}
	
	public void setCabecera(String nombre, ArrayList<String> cabecera) {
		indexRow = 0;
		sheet = workbook.createSheet(nombre);
		XSSFRow row = sheet.createRow(indexRow);
		indexRow++;
		int index = 0;
		for (String head : cabecera) {
			XSSFCell cell = row.createCell(index);
			cell.setCellValue(head);
			cell.setCellStyle(styleCabecera);
			index++;
		}
	}
	
	public void setReporte(ArrayList<ArrayList<String>> datos) {
		for (ArrayList<String> fila : datos) {
			XSSFRow row = sheet.createRow(indexRow);
			int indexCell = 0;
			for (String celda : fila) {
				XSSFCell cell = row.createCell(indexCell);
				cell.setCellValue(celda);
				cell.setCellStyle(styleCelda);
				indexCell++;
			}
			indexRow++;
		}
		XSSFRow row = sheet.getRow(indexRow-1);
		for (int i = 0; i < row.getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
	}
	
	public void close() {
		try {
			XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
			if (original != null) {
				original.close();
			}
			workbook.write(nuevo);
			nuevo.close();
		}catch (IOException ex) {
            System.out.println(ex.getMessage());
		}
	}
}
