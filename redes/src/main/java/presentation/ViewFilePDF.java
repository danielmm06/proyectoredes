package presentation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ViewFilePDF
 */
@WebServlet("/ViewFilePDF")
public class ViewFilePDF extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String path = "C:/Users/" + System.getProperty("user.name") + "/Documents/"; //PRUEBAS
//	private static final String path = "/home/Documentos/"; //SERVIDOR
    
    public ViewFilePDF() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.setCharacterEncoding("UTF-8");
		String file = request.getParameter("path");        
		File documento = new File(path+file);
        FileInputStream archivo = new FileInputStream(documento.getPath());
        int tamanoInput = archivo.available();
        byte[] datosPDF = new byte[tamanoInput];
        archivo.read(datosPDF, 0, tamanoInput);

		Path path = Paths.get(documento.getPath());
		response.setHeader("Content-disposition", "inline; filename=" + documento.getName());
		response.setContentType(Files.probeContentType(path));
        response.setContentLength(tamanoInput);
        response.getOutputStream().write(datosPDF);
    
        archivo.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
