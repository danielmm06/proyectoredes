/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import entity.Usuario;
import java.io.IOException;

import persistence.*;

/**
 *
 * @author Daniel
 */
public class App {

    // ConexiÃ³n base de datos
    public static final String HOST_DB = "127.0.0.1:3306"; // Host:Port DB
    public static final String NAME_DB = "dbredes"; // Name DB
    public static final String USER_DB = "root"; // User DB
//    public static final String PASS_DB = "123qwe"; // Password DB Servidor 
    public static final String PASS_DB = "root123"; // Password DB

    // Parametros del sistema
    public static final String nameProyect = "Proyecto - Redes"; // Name Proyect

    // Manejador de Base de datos
    public static DataBase DB; // DB Handler
    public static boolean printSQL = false; // Printer SQL in DAO

    // Ubicaciones path
    public static String pathURL; // Url Web of Proyect
    public static String pathPC; // Local path of folder "Downloads"

    // Clave secreta para el password
//    public static final String SECRET_PASS = "R3d3s"; // Password 
    // Mensajes de Error
    public static String errorLogin; // Error Login Message
    public static String errorSesion; // Error Old Session Message

    // --------- INSTANCIAS ---------//
    public static PersonaDAO PersonaDAO;
    public static TarjetaDAO TarjetaDAO;
    public static RegistroDAO RegistroDAO;
    public static LectorDAO LectorDAO;
    public static BicDAO BicDAO;
    public static Persona_tarjetaDAO Persona_tarjetaDAO;
    public static RolDAO RolDAO;
    public static UsuarioDAO UsuarioDAO;

    // Conectar con Base de Datos
    public static void OpenConnection() {
        DB = DataBase.CreateInstance();
        CreateInstancesDAO();
        DB.OpenConnection();
    }

    public static void CloseConnection() throws IOException {
        DB.CloseConnection();
    }

    public static void AutoCommit() {
        DB.AutoCommit();
    }

    public static void NoAutoCommit() {
        DB.NoAutoCommit();
    }

    public static void CreateInstancesDAO() {
        PersonaDAO = new PersonaDAO();
        TarjetaDAO = new TarjetaDAO();
        RegistroDAO = new RegistroDAO();
        LectorDAO = new LectorDAO();
        BicDAO = new BicDAO();
        Persona_tarjetaDAO = new Persona_tarjetaDAO();
        RolDAO = new RolDAO();
        UsuarioDAO = new UsuarioDAO();

    }

//     Validar Usuario
    public static boolean AuthUser(Usuario usuario, String password) {
        boolean valido = false;
        if (usuario.getNomb_user() != null) {
            if (!usuario.getNomb_user().equals("")) {
                Usuario clave = App.UsuarioDAO.get(usuario.getNomb_user());
                if (clave.getPass_user().equals(password)) {
                    valido = true;
                    errorLogin = "";
                } else {
                    valido = false;
                    errorLogin = "Usuario o contraseña equivocados";
                }
            } else {
                valido = false;
                errorLogin = "Usuario no existe";
            }
        } else {
            valido = false;
        }

        return valido;
    }

    // Cifrador MD5
//    public static String MD5(String cadena) {
//        String MD5 = "";
//        try {
//            MD5 = hash(cadena);
//        } catch (Exception e) {
//            throw new Error("Error al cifrar cadena");
//        }
//        return MD5;
//    }
//    private static String hash(String clear) {
//        try {
//            MessageDigest md = MessageDigest.getInstance("MD5");
//            md.reset();
//            byte[] b = md.digest(clear.getBytes());
//            int size = b.length;
//            StringBuffer h = new StringBuffer(size);
//            for (int i = 0; i < size; i++) {
//                int u = b[i] & 255;
//                if (u < 16) {
//                    h.append("0" + Integer.toHexString(u));
//                } else {
//                    h.append(Integer.toHexString(u));
//                }
//            }
//            return h.toString();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
}
