<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Sidebar -->
    <ul class="sidebar navbar-nav">
        <li class="nav-item active">
        <a class="nav-link" href="Usuario">
          <i class="fas fa-fw fa-file-alt"></i>
          <span>Registro Usuario</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="Rol">
          <i class="fas fa-fw fa-file-alt"></i>
          <span>Registro Rol</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="Lector">
          <i class="fas fa-fw fa-file-alt"></i>
          <span>Registro Lector</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="Tarjeta">
          <i class="fas fa-fw fa-file-alt"></i>
          <span>Registro Tarjeta</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="Registros">
          <i class="fas fa-fw fa-file-alt"></i>
          <span>Ver Registros</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="Persona">
          <i class="fas fa-fw fa-file-alt"></i>
          <span>Registro Persona</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="Persona_tarjeta">
          <i class="fas fa-fw fa-file-alt"></i>
          <span>Asignación de tarjeta</span>
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="Bitacora">
          <i class="fas fa-fw fa-file-alt"></i>
          <span>Bitácora</span>
        </a>
      </li>
      
      
    </ul>
