<%@page contentType="text/html" language="java" pageEncoding="UTF-8" import="java.util.*" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags/template" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<t:Estructura_Sistema>
    <jsp:attribute name="title">
        <c:out value="${title}" />
    </jsp:attribute>
    <jsp:attribute name="styles">
        <!-- Styles -->
        <!-- /Styles -->
    </jsp:attribute>
    <jsp:attribute name="scripts">
        <!-- Scripts -->
        <script src="views/js/jquery-3.3.1.min.js" type="text/javascript"></script>
    </jsp:attribute>

    <jsp:body>
        <div class="container-fluid">

            <div class="col-xs-12 col-md-12">
                <div class="col-middle">
                    <div class="text-center">
                        <div align="center">
                            <br><br><br>
                            <h1><i class="fa fa-database" aria-hidden="true"></i> Bienvenido al Sistema de Control de Acceso</h1>
                            <br><br>
                            <img src="views/pictures/tar.png" alt="Logo - Unillanos">
                            <br>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- /Content -->
    </jsp:body>
</t:Estructura_Sistema>