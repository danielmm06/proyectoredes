<%@page contentType="text/html" language="java" pageEncoding="UTF-8" import="java.util.*" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags/template" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<t:Estructura_Sistema>
    <jsp:attribute name="title">
        <c:out value="${title}" />
    </jsp:attribute>
    <jsp:attribute name="styles">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="views/Assets/lib/bootstrapTable/css/bootstrap-table.min.css">
        <link rel="stylesheet" type="text/css" href="views/Assets/lib/dataTables/css/buttons.bootstrap.min.css"> 
        <link rel="stylesheet" type="text/css" href="views/css/Reportes.css"> 
        <!-- /Styles -->
    </jsp:attribute>
    <jsp:attribute name="scripts">
        <!-- Scripts -->
        <script src="views/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="views/Assets/lib/bootstrapTable/js/bootstrap-table.min.js"></script>
        <script type="text/javascript" src="views/Assets/lib/bootstrapTable/js/bootstrap-table-locale-all.min.js"></script>
        <script type="text/javascript" src="views/js/bic.js"></script>
        <!-- /Scripts -->
    </jsp:attribute>

    <jsp:body>
        <div class="container-fluid">


            <div class="card-header">                    
                <h2 align="center"><i class="fa fa-file-alt" aria-hidden="true"></i> Bitácora</h2>
            </div>

            <div class="card-body  mx-auto mt-2">  

                <div class="tab-content" id="ocultar">
                    <div class="ln_solid"></div>
                    <div id="tab-cvd-creadas" class="jp-margin-top-50">   
                        <div id="toolbar">
                        </div>
                        <table id="ListaBic" 
                               data-search="true" 
                               data-sort-stable="true"
                               data-pagination="true" 
                               data-click-to-select="true"
                               data-toolbar="#toolbar" 
                               data-show-refresh="true"
                               data-show-columns="true"
                               data-buttons-class="primary"
                               >
                        </table>        						
                    </div>
                </div>

            </div>


        </div>
        <!-- /Content -->
    </jsp:body>
</t:Estructura_Sistema>