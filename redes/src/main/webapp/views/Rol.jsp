<%@page contentType="text/html" language="java" pageEncoding="UTF-8" import="java.util.*" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags/template" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<t:Estructura_Sistema>
    <jsp:attribute name="title">
        <c:out value="${title}" />
    </jsp:attribute>
    <jsp:attribute name="styles">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="views/Assets/lib/bootstrapTable/css/bootstrap-table.min.css">
        <link rel="stylesheet" type="text/css" href="views/Assets/lib/dataTables/css/buttons.bootstrap.min.css"> 
        <link rel="stylesheet" type="text/css" href="views/css/Reportes.css"> 
        <!-- /Styles -->
    </jsp:attribute>
    <jsp:attribute name="scripts">
        <!-- Scripts -->
        <script src="views/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="views/Assets/lib/bootstrapTable/js/bootstrap-table.min.js"></script>
        <script type="text/javascript" src="views/Assets/lib/bootstrapTable/js/bootstrap-table-locale-all.min.js"></script>
        <script type="text/javascript" src="views/js/rol.js"></script>
        <!-- /Scripts -->
    </jsp:attribute>

    <jsp:body>
        <div class="container-fluid">


            <div class="card-header">                    
                <h2 align="center"><i class="fa fa-file-alt" aria-hidden="true"></i> Registro de Rol</h2>
            </div>

            <div class="card-body  mx-auto mt-2">  

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12">
                        <button type="button" id="crear" class="btn btn-success"><i class="fa fa-plus-circle"></i> Crear</button>
                        <button type="button" id="update" class="btn btn-info"><i class="fa fa-check-square"></i> Actualizar</button>
                        <button type="button" id="delete" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
                    </div>
                    <div class="col-md-12 col-lg-12 col-xs-12" id="divcrear" style="display:none;">
                        <div class="row">

                            <div class="col-md-4 col-sm-4 col-xs-12" >
                                <br>
                                <label for="rol">Ingrese el Rol</label>
                                <input type="text" id="rol" name="rol" class="form-control" placeholder="Ingrese el rol" >
                            </div>

                        </div>

                        <div class="col-md-12 col-xs-12 " id="botonCrear">
                            <br>
                            <button type="button" id="botonCrear" class="btn btn-success">
                                <i class="fa fa-plus-circle"></i><span style="padding: 9px 12px; margin: 0 auto; font-size: 14px;">Crear</span>
                            </button>
                        </div>

                    </div>

                    <div class="col-md-12 col-lg-12 col-xs-12" id="Update" style="display:none;">
                        <div class="row">

                            <div class="col-md-4 col-sm-4 col-xs-12" >
                                <br>
                                <label for="rol">Ingrese el rol actual</label>
                                <input type="text" id="rolactual" name="rolactual" class="form-control" placeholder="Ingrese el rol actual" >
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-12" >
                                <br>
                                <label for="rol">Ingrese el nuevo rol</label>
                                <input type="text" id="rolnuevo" name="rolnuevo" class="form-control" placeholder="Ingrese el nuevo rol" >
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-xs-12 " id="botonActualizar">
                            <br>
                            <button type="button" id="botonUpdate" class="btn btn-primary">
                                <i class="fa fa-check-square"></i><span style="padding: 9px 12px; margin: 0 auto; font-size: 14px;">Actualizar</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4 col-xs-12" id="Eliminar" style="display:none;">
                        <div class="form-group">
                            <br>
                            <label for="rol">Ingrese el Rol a eliminar</label>
                            <input type="text" id="roldel" name="roldel" class="form-control" placeholder="Ingrese el rol a eliminar" >
                        </div>
                        <div class="col-md-12 col-xs-12 " id="botonEliminar">
                            <button type="button" id="botonDelete" class="btn btn-danger">
                                <i class="fa fa-trash"></i><span style="padding: 9px 12px; margin: 0 auto; font-size: 14px;">Eliminar</span>
                            </button>
                        </div>
                    </div>

                </div>

                <div class="tab-content" id="ocultar">
                    <div class="ln_solid"></div>
                    <div id="tab-cvd-creadas" class="jp-margin-top-50">   
                        <div id="toolbar">
                        </div>
                        <table id="ListaRol" 
                               data-search="true" 
                               data-sort-stable="true"
                               data-pagination="true" 
                               data-click-to-select="true"
                               data-toolbar="#toolbar" 
                               data-show-columns="true" 
                               data-buttons-class="primary"
                               >
                        </table>        						
                    </div>
                </div>

            </div>
        </div>
        <!-- /Content -->
    </jsp:body>
</t:Estructura_Sistema>