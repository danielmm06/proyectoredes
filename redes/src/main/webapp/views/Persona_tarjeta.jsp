<%@page contentType="text/html" language="java" pageEncoding="UTF-8" import="java.util.*" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags/template" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<t:Estructura_Sistema>
    <jsp:attribute name="title">
        <c:out value="${title}" />
    </jsp:attribute>
    <jsp:attribute name="styles">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="views/Assets/lib/bootstrapTable/css/bootstrap-table.min.css">
        <link rel="stylesheet" type="text/css" href="views/Assets/lib/dataTables/css/buttons.bootstrap.min.css"> 
        <link rel="stylesheet" type="text/css" href="views/css/Reportes.css"> 
        <!-- /Styles -->
    </jsp:attribute>
    <jsp:attribute name="scripts">
        <!-- Scripts -->
        <script src="views/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="views/Assets/lib/bootstrapTable/js/bootstrap-table.min.js"></script>
        <script type="text/javascript" src="views/Assets/lib/bootstrapTable/js/bootstrap-table-locale-all.min.js"></script>
        <script type="text/javascript" src="views/js/persona_tarjeta.js"></script>
        <!-- /Scripts -->
    </jsp:attribute>

    <jsp:body>
        <div class="container-fluid">


            <div class="card-header">                    
                <h2 align="center"><i class="fa fa-file-alt" aria-hidden="true"></i> Asignación de tarjeta</h2>
            </div>

            <div class="card-body  mx-auto mt-2">  

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12">
                        <button type="button" id="crear" class="btn btn-success"><i class="fa fa-plus-circle"></i> Asignación</button>
                        <button type="button" id="delete" class="btn btn-danger"><i class="fa fa-trash"></i> Devolución</button>
                    </div>
                    <div class="col-md-12 col-lg-12 col-xs-12" id="divcrear" style="display:none;">
                        <div class="row">

                            <div class="col-md-4 col-sm-4 col-xs-12" >
                                <br>
                                <label for="lector">Ingrese el documento</label>
                                <input type="text" id="doc" name="doc" class="form-control" placeholder="Ingrese el documento" >
                            </div>
                            
                             <div class="col-md-4 col-sm-4 col-xs-12" >
                                <br>
                                <label for="lector">Seleccione la tarjeta</label>
                                <select id="tipo"  name="tipo" class="form-control selectpicker">
                                    <option value="1" disabled selected>Seleccione una opción</option>
                                    <option value="Llavero">Tarjeta 1</option>
                                    <option value="Tarjeta">Tarjeta 2</option>
                                </select>
                            </div>

                        </div>

                        <div class="col-md-12 col-xs-12 " id="botonCrear">
                            <br>
                            <button type="button" id="botonCrear" class="btn btn-success">
                                <i class="fa fa-plus-circle"></i><span style="padding: 9px 12px; margin: 0 auto; font-size: 14px;">Asignar</span>
                            </button>
                        </div>

                    </div>

                    <div class="col-md-4 col-lg-4 col-xs-12" id="Eliminar" style="display:none;">
                        <div class="form-group">
                            <br>
                            <label for="lector">Ingrese el documento</label>
                            <input type="text" id="deletepers" name="deletepers" class="form-control" placeholder="Ingrese el documento" >
                        </div>
                        <div class="col-md-12 col-xs-12 " id="botonEliminar">
                            <button type="button" id="botonDelete" class="btn btn-danger">
                                <i class="fa fa-trash"></i><span style="padding: 9px 12px; margin: 0 auto; font-size: 14px;">Devolver</span>
                            </button>
                        </div>
                    </div>

                </div>

                <div class="tab-content" id="ocultar">
                    <div class="ln_solid"></div>
                    <div id="tab-cvd-creadas" class="jp-margin-top-50">   
                        <div id="toolbar">
                        </div>
                        <table id="ListaPersona_tarjeta" 
                               data-search="true" 
                               data-sort-stable="true"
                               data-pagination="true" 
                               data-click-to-select="true"
                               data-toolbar="#toolbar" 
                               data-show-columns="true" 
                               data-buttons-class="primary"
                               >
                        </table>        						
                    </div>
                </div>

            </div>
        </div>
        <!-- /Content -->
    </jsp:body>
</t:Estructura_Sistema>