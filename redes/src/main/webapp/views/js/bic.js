$(document).ready(function () {
    $('#ListaBic').bootstrapTable({
        columns: [
            {
                field: 'fecha',
                title: 'Fecha',
                sortable: true
            },
            {
                field: 'usuario',
                title: 'Usuario'

            },
            {
                field: 'nomb_table',
                title: 'Tabla'

            },
            {
                field: 'oper',
                title: 'Operación'

            },
            {
                field: 'valor_v',
                title: 'Valor_v'

            },
            {  
                field: 'valor_n',
                title: 'Valor_n'

            }],
        locale: 'es-SP',
        classes: 'table table-bordered table-striped table-condensed',
        iconSize: 'md',
        url: 'Ajax',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'text',
        queryParams: function (params) {
            params.value = 'ListaBic';
            return params;
        },
        responseHandler: function (dataresponse) {
            return JSON.parse(dataresponse);
        }
    });

});








