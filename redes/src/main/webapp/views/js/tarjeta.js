$(document).ready(function () {
    $('#crear').click(function () {
        crear();
    });
    $('#update').click(function () {
        update();
    });
    $('#delete').click(function () {
        deletes();
    });
    $('#botonCrear').click(function () {
        botonCrear();
    });
    $('#botonUpdate').click(function () {
        botonUpdate();
    });
    $('#botonDelete').click(function () {
        botonDelete();
    });

    $('#ListaTarjeta').bootstrapTable({
        columns: [
            {
                field: 'id',
                title: 'ID',
                sortable: true
            },
            {
                field: 'cod',
                title: 'Cod_Trjeta'

            },
            {
                field: 'tipo',
                title: 'Tipo'

            },
            {
                field: 'estado',
                title: 'Estado'

            }],
        locale: 'es-SP',
        classes: 'table table-bordered table-striped table-condensed',
        iconSize: 'md',
        url: 'Ajax',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'text',
        queryParams: function (params) {
            params.value = 'ListaTarjeta';
            return params;
        },
        responseHandler: function (dataresponse) {
            return JSON.parse(dataresponse);
        }
    });

});

function crear() {
    $('#divcrear').show();
    $('#Update').hide();
    $('#Eliminar').hide();
}

function update() {
    $('#divcrear').hide();
    $('#Update').show();
    $('#Eliminar').hide();
}

function deletes() {
    $('#divcrear').hide();
    $('#Update').hide();
    $('#Eliminar').show();
}

function botonCrear() {
    var cod = ($('#cod').val()).toUpperCase();
    var tipo = $('#tipo').val();
    var estado = $('#estado').val();
    var enviar = cod + "," + tipo + "," + estado;
//    alert(enviar);
    var R = AJAX(enviar, null, 'crearTarjeta', false);
    alert(R);
    window.location = location.href;
}

function botonUpdate() {
    var codIpdate = ($('#codIpdate').val()).toUpperCase();
    var estadoUpdate = $('#estadoUpdate').val();
    var enviar = codIpdate + "," + estadoUpdate;
    var R2 = AJAX(enviar, null, 'UpdateTarjeta', false);
    alert(R2);
    window.location = location.href;
}

function botonDelete() {
    var coddelete = ($('#coddelete').val()).toUpperCase();
    var R3 = AJAX(coddelete, null, 'deleteTarjeta', false);
    alert(R3);
    window.location = location.href;
}





