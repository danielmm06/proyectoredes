$(document).ready(function () {
    $('.fecha').datetimepicker({minView: 2, format: 'dd/mm/yyyy', autoclose: true});
    $('#Consultar').click(function () {
        consultar2()
    });
    $('#limpiar').click(function () {
        limpiar()
    });

});


function consultar2() {
    var fecha = $('#fini').val();
    if (fecha != "") {
        var FechaNueva = arreglarFecha(fecha);
        $("#fini").val(FechaNueva);
    }


    var fecha2 = $('#ffin').val();
    if (fecha2 != "") {
        var FechaNueva2 = arreglarFecha(fecha2);
        $("#ffin").val(FechaNueva2);
    }


    var fini = $('#fini').val();
    var fecha ='';
    if (fini != "") {
        fecha = restaFecha(fini);
    }
    
    var ffin = $('#ffin').val();
    var hini = $('#hini').val();
    var hfin = $('#hfin').val();

    $('#ListaReg').bootstrapTable('destroy');

    $('#ListaReg').bootstrapTable({
        columns: [
            {
                field: 'fecha',
                title: 'Fecha',
                sortable: true
            },
            {
                field: 'hora',
                title: 'Hora',
                sortable: true
            },
            {
                field: 'lector',
                title: 'Lector',
                sortable: true
            },
            {
                field: 'cod_terjeta',
                title: 'Cod Terjeta',
                sortable: true
            },
            {
                field: 'nombre',
                title: 'Nombre',
                sortable: true
            }],
        locale: 'es-SP',
        classes: 'table table-bordered table-striped table-condensed',
        iconSize: 'md',
        url: 'Ajax',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'text',
        queryParams: function (params) {
            params.value = 'ListRegistros';
            params.fini = fecha;
            params.ffin = ffin;
            params.hini = hini;
            params.hfin = hfin;
            return params;
        },
        responseHandler: function (dataresponse) {
            if (dataresponse === "[]") {
                $('#ocultar').addClass("hidden");
                return JSON.parse(dataresponse);
            } else {
                $('#ocultar').removeClass("hidden");
                return JSON.parse(dataresponse);
            }
        }
    });
    

    $('#toolbar').show();

}



function arreglarFecha(fecha) {
    fecha = fecha.split("/");
    var nuevaFecha = '';
    for (var i = fecha.length - 1; i >= 0; i--) {
        nuevaFecha += '-' + fecha[i];
    }
    nuevaFecha = nuevaFecha.substring(1);
    return nuevaFecha;
}

function restaFecha(fecha) {
    fecha = fecha.split("-");
    var nuevaFecha = '';
    var dia = fecha[2];
    nuevaFecha = fecha[0] + '-' + fecha[1] + '-' + (dia - 1);

    return nuevaFecha;
}

function limpiar() {
    $('#fini').val('');
    $('#ffin').val('');
    $('#hini').val('');
    $('#hfin').val('');
    $('#ocultar').addClass('hidden');
    $('#ListaReg').bootstrapTable('destroy');
}


$('#export').click(function(){
	var cabecera="";
	var aux="";
	//Se obtiene los datos Visibles de la tabla
	var datosTitulos = $('#ListaReg').bootstrapTable('getVisibleColumns').map(function(row){
		return [row.field,row.title];
	});
	//Obtienen los datos para la cabecera 
	for (var i = 0; i < datosTitulos.length; i++) {
		cabecera += ","+datosTitulos[i][1];
	}
	cabecera = cabecera.substring(1);
	
	var datos = $('#ListaReg').bootstrapTable('getData', false).map(function(row){
		var filas = [];
		for (var i = 0; i < datosTitulos.length; i++) {
			filas.push(row[datosTitulos[i][0]])
		}
		return filas;
	});
	
	//se Obtienen los datos de la tabla
	for (var i = 0; i < datos.length; i++) {
		var temp = "";
		for (var j = 0; j < datos[i].length; j++) {
			temp += "<,>" + datos[i][j];
		}
		temp = temp.substring(3);
		aux += "<->" + temp;
	}
	aux = aux.substring(3);
	
	var datosEnviar = cabecera+"<_>"+aux;
	
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", "Ajax");
	form.setAttribute("target", "_self");

	var hiddenField = document.createElement("input"); 
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "content");
	hiddenField.setAttribute("value", datosEnviar);
	form.appendChild(hiddenField);
	
	var URL = AJAX(form, null, 'GenerarExcel', false);
	window.open('ViewFilePDF?path='+URL, '_blank');
	
});