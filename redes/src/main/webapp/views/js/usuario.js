$(document).ready(function () {
    $('#crear').click(function () {
        crear();
    });
    $('#update').click(function () {
        update();
    });
    $('#delete').click(function () {
        deletes();
    });
    $('#botonCrear').click(function () {
        botonCrear();
    });
    $('#botonUpdate').click(function () {
        botonUpdate();
    });
    $('#botonDelete').click(function () {
        botonDelete();
    });

    $('#ListaUsuario').bootstrapTable({
        columns: [
            {
                field: 'nomb_user',
                title: 'Usuario',
                sortable: true
            },
            {
                field: 'doc',
                title: 'Documento'

            },
            {
                field: 'pers',
                title: 'Nombre Persona'

            }],
        locale: 'es-SP',
        classes: 'table table-bordered table-striped table-condensed',
        iconSize: 'md',
        url: 'Ajax',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'text',
        queryParams: function (params) {
            params.value = 'ListaUsuario';
            return params;
        },
        responseHandler: function (dataresponse) {
            return JSON.parse(dataresponse);
        }
    });

});

function crear() {
    $('#divcrear').show();
    $('#Update').hide();
    $('#Eliminar').hide();
}

function update() {
    $('#divcrear').hide();
    $('#Update').show();
    $('#Eliminar').hide();
}

function deletes() {
    $('#divcrear').hide();
    $('#Update').hide();
    $('#Eliminar').show();
}

function botonCrear() {
    var user = $('#usuario').val();
    var pass = $('#pass').val();
    var doc = $('#doc').val();
    var enviar = user + "," + pass + "," + doc;
    var R = AJAX(enviar, null, 'crearUsuario', false);
    alert(R);
    window.location = location.href;
}

function botonUpdate() {
    var user = $('#usuarioup').val();
    var pass = $('#passup').val();
    var enviar = user + "," + pass;
    var R2 = AJAX(enviar, null, 'updateUser', false);
    alert(R2);
    window.location = location.href;
}

function botonDelete() {
    var user = $('#deleteuser').val();
    var R3 = AJAX(user, null, 'eliminaruser', false);
    alert(R3);
    window.location = location.href;
}


