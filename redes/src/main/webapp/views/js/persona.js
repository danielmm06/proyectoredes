$(document).ready(function () {
    $('#crear').click(function () {
        crear();
    });
    $('#update').click(function () {
        update();
    });
    $('#delete').click(function () {
        deletes();
    });
    $('#botonCrear').click(function () {
        botonCrear();
    });
    $('#botonUpdate').click(function () {
        botonUpdate();
    });
    $('#botonDelete').click(function () {
        botonDelete();
    });

    $('#ListaPersona').bootstrapTable({
        columns: [
            {
                field: 'id_pers',
                title: 'Persona',
                sortable: true
            },
            {
                field: 'nombre',
                title: 'Nombre Persona'

            },
            {
                field: 'apellido',
                title: 'Apellido Persona'

            },
            {
                field: 'doc',
                title: 'Documento persona'

            }],
        locale: 'es-SP',
        classes: 'table table-bordered table-striped table-condensed',
        iconSize: 'md',
        url: 'Ajax',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'text',
        queryParams: function (params) {
            params.value = 'ListaUsuario';
            return params;
        },
        responseHandler: function (dataresponse) {
            return JSON.parse(dataresponse);
        }
    });

});

function crear() {
    $('#divcrear').show();
    $('#Update').hide();
    $('#Eliminar').hide();
}

function update() {
    $('#divcrear').hide();
    $('#Update').show();
    $('#Eliminar').hide();
}

function deletes() {
    $('#divcrear').hide();
    $('#Update').hide();
    $('#Eliminar').show();
}

function botonCrear() {
//    var user = $('#usuario').val();
//    var pass = $('#pass').val();
//    var doc = $('#doc').val();
//    var enviar = user + "," + pass + "," + doc;
//    var R = AJAX(enviar, null, 'crearPersona', false);
//    alert(R);
//    window.location = location.href;
}

function botonUpdate() {
//    var user = $('#usuarioup').val();
//    var pass = $('#passup').val();
//    var enviar = user + "," + pass;
//    var R2 = AJAX(enviar, null, 'updatePersona', false);
//    alert(R2);
//    window.location = location.href;
}

function botonDelete() {
//    var user = $('#deleteuser').val();
//    var R3 = AJAX(user, null, 'eliminarPersona', false);
//    alert(R3);
//    window.location = location.href;
}


