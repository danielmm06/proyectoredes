$(document).ready(function () {
    $('#crear').click(function () {
        crear();
    });
    $('#update').click(function () {
        update();
    });
    $('#delete').click(function () {
        deletes();
    });
    $('#botonCrear').click(function () {
        botonCrear();
    });
    $('#botonUpdate').click(function () {
        botonUpdate();
    });
    $('#botonDelete').click(function () {
        botonDelete();
    });

    $('#ListaLector').bootstrapTable({
        columns: [
            {
                field: 'id',
                title: 'ID',
                sortable: true
            },
            {
                field: 'ubicacion',
                title: 'Ubicacion'

            }],
        locale: 'es-SP',
        classes: 'table table-bordered table-striped table-condensed',
        iconSize: 'md',
        url: 'Ajax',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'text',
        queryParams: function (params) {
            params.value = 'ListaLec';
            return params;
        },
        responseHandler: function (dataresponse) {
            return JSON.parse(dataresponse);
        }
    });

});

function crear() {
    $('#divcrear').show();
    $('#botonCrear').show();
    $('#botonEliminar').hide();
    $('#Update').hide();
}

function update() {
    $('#divcrear').show();
    $('#Update').show();
    $('#botonActualizar').show();
    $('#botonCrear').hide();
    $('#botonEliminar').hide();
}

function deletes() {
    $('#divcrear').show();
    $('#botonEliminar').show();
    $('#botonCrear').hide();
    $('#Update').hide();
    $('#botonActualizar').hide();
}

function botonCrear() {
    var crear = $('#lector').val();
    var R = AJAX(crear, null, 'crearLector', false);
    window.location = location.href;
}

function botonUpdate() {
    var crear2 = $('#lector').val();
    var update = $('#lector2').val(); 
    var R2 = AJAX(crear2+","+update, null, 'updateLector', false);
    window.location = location.href;
}

function botonDelete() {
    var deletes = $('#lector').val();
    var R3 = AJAX(deletes, null, 'eliminarLector', false);
    window.location = location.href;
}