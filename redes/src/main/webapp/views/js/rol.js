$(document).ready(function () {
    $('#crear').click(function () {
        crear();
    });
    $('#update').click(function () {
        update();
    });
    $('#delete').click(function () {
        deletes();
    });
    $('#botonCrear').click(function () {
        botonCrear();
    });
    $('#botonUpdate').click(function () {
        botonUpdate();
    });
    $('#botonDelete').click(function () {
        botonDelete();
    });

    $('#ListaRol').bootstrapTable({
        columns: [
            {
                field: 'id',
                title: 'ID',
                sortable: true
            },
            {
                field: 'rol',
                title: 'Rol'

            }],
        locale: 'es-SP',
        classes: 'table table-bordered table-striped table-condensed',
        iconSize: 'md',
        url: 'Ajax',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'text',
        queryParams: function (params) {
            params.value = 'ListaRol';
            return params;
        },
        responseHandler: function (dataresponse) {
            return JSON.parse(dataresponse);
        }
    });

});

function crear() {
    $('#divcrear').show();
    $('#Update').hide();
    $('#Eliminar').hide();
}

function update() {
    $('#divcrear').hide();
    $('#Update').show();
    $('#Eliminar').hide();
}

function deletes() {
    $('#divcrear').hide();
    $('#Update').hide();
    $('#Eliminar').show();
}

function botonCrear() {
    var rol = $('#rol').val();
    var R = AJAX(rol, null, 'crearRol', false);
    alert(R);
    window.location = location.href;
}

function botonUpdate() {
    var rol = $('#rolactual').val();
    var rolnuevo = $('#rolnuevo').val();
    var enviar = rol + "," + rolnuevo;
    var R2 = AJAX(enviar, null, 'updateRol', false);
    alert(R2);
    window.location = location.href;
}

function botonDelete() {
    var rol = $('#roldel').val();
    var R3 = AJAX(rol, null, 'roldel', false);
    alert(R3);
    window.location = location.href;
}


