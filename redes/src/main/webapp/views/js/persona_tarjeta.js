$(document).ready(function () {
    $('#crear').click(function () {
        crear();
    });
    $('#delete').click(function () {
        deletes();
    });
    $('#botonCrear').click(function () {
        botonCrear();
    });
    $('#botonDelete').click(function () {
        botonDelete();
    });

    $('#ListaPersona_tarjeta').bootstrapTable({
        columns: [
        	{
                field: 'fech_asig',
                title: 'Fecha de asignación',
                sortable: true
            },
            {
                field: 'fech_dev',
                title: 'Fecha de devolución',
                sortable: true

            },
            {
                field: 'id_pers',
                title: 'Persona',
            },
            {
                field: 'id_tarj',
                title: 'Nº tarjeta'
            }
            ],
        locale: 'es-SP',
        classes: 'table table-bordered table-striped table-condensed',
        iconSize: 'md',
        url: 'Ajax',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'text',
        queryParams: function (params) {
            params.value = 'ListaUsuario';
            return params;
        },
        responseHandler: function (dataresponse) {
            return JSON.parse(dataresponse);
        }
    });

});

function crear() {
    $('#divcrear').show();
    $('#Update').hide();
    $('#Eliminar').hide();
}


function deletes() {
    $('#divcrear').hide();
    $('#Update').hide();
    $('#Eliminar').show();
}

function botonCrear() {
//    var user = $('#usuario').val();
//    var pass = $('#pass').val();
//    var doc = $('#doc').val();
//    var enviar = user + "," + pass + "," + doc;
//    var R = AJAX(enviar, null, 'crearPersona', false);
//    alert(R);
//    window.location = location.href;
}

function botonUpdate() {
//    var user = $('#usuarioup').val();
//    var pass = $('#passup').val();
//    var enviar = user + "," + pass;
//    var R2 = AJAX(enviar, null, 'updatePersona', false);
//    alert(R2);
//    window.location = location.href;
}

function botonDelete() {
//    var user = $('#deleteuser').val();
//    var R3 = AJAX(user, null, 'eliminarPersona', false);
//    alert(R3);
//    window.location = location.href;
}


