<%@page contentType="text/html" language="java" pageEncoding="UTF-8" import="java.util.*" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags/template" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<t:Estructura_Sistema>
    <jsp:attribute name="title">
        <c:out value="${title}" />
    </jsp:attribute>
    <jsp:attribute name="styles">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="views/Assets/lib/bootstrapTable/css/bootstrap-table.min.css">
        <link rel="stylesheet" type="text/css" href="views/Assets/lib/dataTables/css/buttons.bootstrap.min.css"> 
        <link rel="stylesheet" type="text/css" href="views/css/bootstrap-datetimepicker.min.css"> 
        <link rel="stylesheet" type="text/css" href="views/css/Reportes.css"> 
        <!-- /Styles -->
    </jsp:attribute>
    <jsp:attribute name="scripts">
        <!-- Scripts -->
        <script src="views/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="views/Assets/lib/bootstrapTable/js/bootstrap-table.min.js"></script>
        <script type="text/javascript" src="views/Assets/lib/bootstrapTable/js/bootstrap-table-locale-all.min.js"></script>
        <!--        <script type="text/javascript" src="views/Assets/lib/dataTables/js/pdfmake.min.js"></script>
                <script type="text/javascript" src="views/Assets/lib/dataTables/js/vfs_fonts.js"></script>-->
        <script type="text/javascript" src="views/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="views/js/prueba.js"></script>
        <!-- /Scripts -->
    </jsp:attribute>

    <jsp:body>
        <div class="container-fluid">


            <div class="card-header">                    
                <h2 align="center"><i class="fa fa-file-alt" aria-hidden="true"></i> Prueba Redes</h2>
            </div>

            <div class="card-body  mx-auto mt-2">  
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <br>
                            <div class="x_content">
                                <div class="form-group" id="formEstudiante" >
                                    <form name="buscarform" id="buscarform" method="post" action="ParametrosDescuentos">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-xs-12">
                                                <h4>Buscar Por Fecha</h4>
                                                <div class="col-lg-8 col-md-8  col-xs-12" >
                                                    <h5>Fecha inicio</h5>
                                                    <input type="text" name="fini"  id="fini" class=" form-control fecha" placeholder="dia/mes/año" >
                                                </div>
                                                <div class="col-lg-8 col-md-8  col-xs-12" >
                                                    <h5>Fecha fin</h5><input type="text" name="ffin"  id="ffin" class=" form-control fecha" placeholder="dia/mes/año" >
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-lg-6 col-xs-12">
                                                <h4>Buscar Por Horas</h4>
                                                <div class="col-lg-8 col-md-8  col-xs-12" >
                                                    <h5>Hora Inicio</h5>
                                                    <!--<input type="text" name="hini"  id="hini" class=" form-control" placeholder="Ingrese la Hora" >-->
                                                    <select id="hini" name="hini" class="form-control selectpicker show-tick limpiar"  >
                                                        <option value=""  selected >Seleccione una hora</option>
                                                        <option value=6:00:00>6:00:00</option>
                                                        <option value=7:00:00>7:00:00</option>
                                                        <option value=8:00:00>8:00:00</option>
                                                        <option value=9:00:00>9:00:00</option>
                                                        <option value=10:00:00>10:00:00</option>
                                                        <option value=11:00:00>11:00:00</option>
                                                        <option value=12:00:00>12:00:00</option>
                                                        <option value=13:00:00>13:00:00</option>
                                                        <option value=14:00:00>14:00:00</option>
                                                        <option value=15:00:00>15:00:00</option>
                                                        <option value=16:00:00>16:00:00</option>
                                                        <option value=17:00:00>17:00:00</option>
                                                        <option value=18:00:00>18:00:00</option>
                                                        <option value=19:00:00>19:00:00</option>
                                                        <option value=20:00:00>20:00:00</option>
                                                        <option value=21:00:00>21:00:00</option>
                                                        <option value=22:00:00>22:00:00</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-8 col-md-8  col-xs-12" >
                                                    <h5>Hora fin</h5>
                                                    <!--<input type="text" name="hfin"  id="hfin" class=" form-control" placeholder="Ingrese la Hora" >-->
                                                    <select id="hfin" name="hfin" class="form-control selectpicker show-tick limpiar"  >
                                                        <option value=""  selected >Seleccione una hora</option>
                                                        <option value=6:00:00>6:00:00</option>
                                                        <option value=7:00:00>7:00:00</option>
                                                        <option value=8:00:00>8:00:00</option>
                                                        <option value=9:00:00>9:00:00</option>
                                                        <option value=10:00:00>10:00:00</option>
                                                        <option value=11:00:00>11:00:00</option>
                                                        <option value=12:00:00>12:00:00</option>
                                                        <option value=13:00:00>13:00:00</option>
                                                        <option value=14:00:00>14:00:00</option>
                                                        <option value=15:00:00>15:00:00</option>
                                                        <option value=16:00:00>16:00:00</option>
                                                        <option value=17:00:00>17:00:00</option>
                                                        <option value=18:00:00>18:00:00</option>
                                                        <option value=19:00:00>19:00:00</option>
                                                        <option value=20:00:00>20:00:00</option>
                                                        <option value=21:00:00>21:00:00</option>
                                                        <option value=22:00:00>22:00:00</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="container-fluid h-100"> 
                                            <div class="row w-100 align-items-center">
                                                <div class="col text-center"> 
                                                    <button type="button" id="Consultar" class="btn btn-success" style="margin-top: 35px;" >Consultar</button>
                                                    <button type="button" id="limpiar" class="btn btn-primary" style="margin-top: 35px;" >Limpiar</button>
                                                </div>
                                            </div>
                                        </div>



                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-content hidden" id="ocultar">
                    <div class="ln_solid"></div>
                    <div id="tab-cvd-creadas" class="jp-margin-top-50">   
                        <div id="toolbar">
                        </div>
                        <table id="ListaReg" 
                               data-search="true" 
                               data-sort-stable="true"
                               data-pagination="true" 
                               data-click-to-select="true"
                               data-toolbar="#toolbar" 
                               data-show-columns="true" 
                               data-buttons-class="primary"
                               >
                        </table>        						
                    </div>
                </div>


            </div>


        </div>
        <!-- /Content -->
    </jsp:body>
</t:Estructura_Sistema>