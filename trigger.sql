create table bic(
codigo serial primary key,
fecha TIMESTAMP DEFAULT now(), 
usuario varchar (512), 
nomb_table varchar(512),
oper varchar(512), 
valor_v varchar(512), 
valor_n varchar(512));


DROP TRIGGER IF EXISTS `dbredes`.`rol_AFTER_INSERT`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `rol_AFTER_INSERT` AFTER INSERT ON `rol` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'rol', 'Agregar', Null, concat(new.id_rol,' - ',new.nomb_rol));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`rol_AFTER_UPDATE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `rol_AFTER_UPDATE` AFTER UPDATE ON `rol` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'rol', 'Actualizar', concat(old.id_rol,' - ',old.nomb_rol), concat(new.id_rol,' - ',new.nomb_rol));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`rol_AFTER_DELETE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `rol_AFTER_DELETE` AFTER DELETE ON `rol` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'rol', 'Eliminar', concat(old.id_rol,' - ',old.nomb_rol), NUll);
END$$
DELIMITER ;


DROP TRIGGER IF EXISTS `dbredes`.`lector_AFTER_INSERT`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `lector_AFTER_INSERT` AFTER INSERT ON `lector` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'lector', 'Agregar', Null, concat(new.id_lect,' - ',new.ubic_lect));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`lector_AFTER_UPDATE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `lector_AFTER_UPDATE` AFTER UPDATE ON `lector` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'lector', 'Actualizar', concat(old.id_lect,' - ',old.ubic_lect), concat(new.id_lect,' - ',new.ubic_lect));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`lector_AFTER_DELETE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `lector_AFTER_DELETE` AFTER DELETE ON `lector` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'lector', 'Eliminar', concat(old.id_lect,' - ',old.ubic_lect), NUll);
END$$
DELIMITER ;


DROP TRIGGER IF EXISTS `dbredes`.`usuario_AFTER_INSERT`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `usuario_AFTER_INSERT` AFTER INSERT ON `usuario` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Agregar', Null, concat(new.nomb_user,' - ',new.id_pers));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`usuario_AFTER_UPDATE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `usuario_AFTER_UPDATE` AFTER UPDATE ON `usuario` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Actualizar', concat(old.nomb_user,' - ',old.id_pers), concat(new.nomb_user,' - ',new.id_pers));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`usuario_AFTER_DELETE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `usuario_AFTER_DELETE` AFTER DELETE ON `usuario` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Eliminar', concat(old.nomb_user,' - ',old.id_pers), NUll);
END$$
DELIMITER ;


DROP TRIGGER IF EXISTS `dbredes`.`persona_AFTER_INSERT`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `persona_AFTER_INSERT` AFTER INSERT ON `persona` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Agregar', Null, concat(new.id_pers,' - ',new.nomb_pers,' - ',new.apell_pers,' - ',new.id_rol));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`persona_AFTER_UPDATE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `persona_AFTER_UPDATE` AFTER UPDATE ON `persona` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Actualizar', concat(old.id_pers,' - ',old.nomb_pers,' - ',old.apell_pers,' - ',old.id_rol), concat(new.id_pers,' - ',new.nomb_pers,' - ',new.apell_pers,' - ',new.id_rol));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`persona_AFTER_DELETE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `persona_AFTER_DELETE` AFTER DELETE ON `persona` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Eliminar', concat(old.id_pers,' - ',old.nomb_pers,' - ',old.apell_pers,' - ',old.id_rol), NUll);
END$$
DELIMITER ;


DROP TRIGGER IF EXISTS `dbredes`.`tarjeta_AFTER_INSERT`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `tarjeta_AFTER_INSERT` AFTER INSERT ON `tarjeta` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Agregar', Null, concat(new.id_tarj,' - ',new.esta_tarj,' - ',new.tipo_tarj));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`tarjeta_AFTER_UPDATE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `tarjeta_AFTER_UPDATE` AFTER UPDATE ON `tarjeta` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Actualizar', concat(old.id_tarj,' - ',old.esta_tarj,' - ',old.tipo_tarj), concat(new.id_tarj,' - ',new.esta_tarj,' - ',new.tipo_tarj));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`tarjeta_AFTER_DELETE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `tarjeta_AFTER_DELETE` AFTER DELETE ON `tarjeta` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Eliminar', concat(old.id_tarj,' - ',old.esta_tarj,' - ',old.tipo_tarj), NUll);
END$$
DELIMITER ;


DROP TRIGGER IF EXISTS `dbredes`.`registro_AFTER_INSERT`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `registro_AFTER_INSERT` AFTER INSERT ON `registro` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Agregar', Null, concat(new.id_regi,' - ',new.fech_regi,' - ',new.hora_regi,' - ',new.id_lect,' - ',new.id_tarj));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`registro_AFTER_UPDATE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `registro_AFTER_UPDATE` AFTER UPDATE ON `registro` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Actualizar', concat(old.id_regi,' - ',old.fech_regi,' - ',old.hora_regi,' - ',old.id_lect,' - ',old.id_tarj), concat(new.id_regi,' - ',new.fech_regi,' - ',new.hora_regi,' - ',new.id_lect,' - ',new.id_tarj));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`registro_AFTER_DELETE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `registro_AFTER_DELETE` AFTER DELETE ON `registro` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Eliminar', concat(old.id_regi,' - ',old.fech_regi,' - ',old.hora_regi,' - ',old.id_lect,' - ',old.id_tarj), NUll);
END$$
DELIMITER ;


DROP TRIGGER IF EXISTS `dbredes`.`persona_tarjeta_AFTER_INSERT`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `persona_tarjeta_AFTER_INSERT` AFTER INSERT ON `persona_tarjeta` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Agregar', Null, concat(new.fech_asig,' - ',new.fech_devo,' - ',new.id_pers,' - ',new.id_tarj));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`persona_tarjeta_AFTER_UPDATE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `persona_tarjeta_AFTER_UPDATE` AFTER UPDATE ON `persona_tarjeta` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Actualizar', concat(old.fech_asig,' - ',old.fech_devo,' - ',old.id_pers,' - ',old.id_tarj), concat(new.fech_asig,' - ',new.fech_devo,' - ',new.id_pers,' - ',new.id_tarj));
END$$
DELIMITER ;

DROP TRIGGER IF EXISTS `dbredes`.`persona_tarjeta_AFTER_DELETE`;

DELIMITER $$
USE `dbredes`$$
CREATE DEFINER=`root`@`localhost` TRIGGER `persona_tarjeta_AFTER_DELETE` AFTER DELETE ON `persona_tarjeta` FOR EACH ROW BEGIN
	INSERT INTO bic(usuario,nomb_table,oper,valor_v,valor_n)
           VALUES (user(), 'usuario', 'Eliminar', concat(old.fech_asig,' - ',old.fech_devo,' - ',old.id_pers,' - ',old.id_tarj), NUll);
END$$
DELIMITER ;