

create table lector(id_lect int, ubic_lect varchar(50));
create table rol(id_rol int, nomb_rol varchar(50));
create table usuario(nomb_user varchar(30),pass_user varchar(30), id_pers int);
create table persona(id_pers int, nomb_pers varchar(50), apell_pers varchar(50), num_documento int, id_rol int);
create table tarjeta(id_tarj int, esta_tarj varchar(50), tipo_tarj varchar(50), cod_tarjeta varchar(50));
create table registro(id_regi int, fech_regi DATE, hora_regi TIME, id_lect int, id_tarj int);
create table persona_tarjeta(fech_asig DATETIME, fech_devo DATETIME, id_pers int, id_tarj int);

Alter table lector add constraint pk_id_lect primary key(id_lect);
Alter table lector modify ubic_lect varchar(50) not null;
Alter table lector modify id_lect int AUTO_INCREMENT;

Alter table rol add constraint pk_id_rol primary key(id_rol);
Alter table rol modify nomb_rol varchar(50) not null;
Alter table rol modify id_rol int AUTO_INCREMENT;

Alter table persona add constraint pk_id_pers primary key(id_pers);
Alter table persona add constraint fk_id_rol foreign key(id_rol) references rol(id_rol);
Alter table persona modify id_pers int AUTO_INCREMENT;

Alter table usuario add constraint pk_nomb_user primary key(nomb_user);
Alter table usuario modify pass_user varchar(30) not null;
Alter table usuario add constraint fk_id_pers foreign key(id_pers) references persona(id_pers);

Alter table tarjeta add constraint pk_id_tarj primary key(id_tarj);
Alter table tarjeta modify esta_tarj varchar(50) not null;
Alter table tarjeta modify id_tarj int AUTO_INCREMENT;

Alter table registro add constraint pk_id_regi primary key(id_regi);
Alter table registro modify id_regi int AUTO_INCREMENT;
Alter table registro modify fech_regi date not null;
Alter table registro modify hora_regi time not null;
Alter table registro add constraint fk_id_lect foreign key(id_lect) references lector(id_lect);
Alter table registro add constraint fk_id_tarj foreign key(id_tarj) references tarjeta(id_tarj);

Alter table persona_tarjeta add constraint fk_id_perspt foreign key(id_pers) references persona(id_pers);
Alter table persona_tarjeta add constraint fk_id_tarjpt foreign key(id_tarj) references tarjeta(id_tarj);
